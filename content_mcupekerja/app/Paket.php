<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
  protected $table = 'paket';
	// protected $fillable = ['name','biaya1','biaya2','biaya3',
	protected $fillable = ['name', 'wajib'];

	public function parameter()
    {
        return $this->belongsToMany('App\Parameter')->withPivot('id');
    }
}
