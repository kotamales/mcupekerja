<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Registrasi extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'registrasi';
    protected $primaryKey = 'id_registrasi';
    // public $timestamps = false;
    

    public function paket()
    {
        return $this->belongsToMany('App\Paket', 'registrasi_paket', 'registrasi_id', 'paket_id');
    }


    public function kota()
    {
    	return $this->belongsToMany('App\Kota', 'registrasi_kota', 'registrasi_id', 'kota_id')
        ->withPivot('pic', 'alamat', 'tanggal', 'jam', 'jml');
    }
}