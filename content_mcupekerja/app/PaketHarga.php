<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaketHarga extends Model
{
  protected $table = 'harga_paket';
	protected $fillable = ['parameter_id','kota_id','harga1','harga2','harga3'];
}
