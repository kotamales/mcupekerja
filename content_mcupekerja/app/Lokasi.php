<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Lokasi extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lokasi';
    protected $primaryKey = 'id_lokasi';
    // public $timestamps = false;
    
}