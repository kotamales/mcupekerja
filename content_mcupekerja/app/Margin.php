<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Margin extends Model
{
    protected $table = 'margin';
    protected $fillable = ['margin'];
}
