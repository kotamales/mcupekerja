<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Kode extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kode_voucher';
    protected $primaryKey = 'id_voucher';
    // public $timestamps = false;
    
}