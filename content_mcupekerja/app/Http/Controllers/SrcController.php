<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Kota;
use App\Notif;
use App\Margin;
use App\Paket;
use App\Parameter;
use DB;
use Datatables;
use Excel;
use Hash;
use Illuminate\Http\Request;
use Response;
use Validator;

class SrcController extends Controller
{

    public function Kota()
    {
        $query = Kota::all();
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="ubah('.$model->id.')" title="ubah Kota" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-pencil"></i></button> <button onClick="hapus('.$model->id.')" title="Hapus Kota" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>';
            })
            ->make(true);
    }
    public function Notif()
    {
        $query = Notif::all();
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="ubah('.$model->id.')" title="ubah Email" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-pencil"></i></button> <button onClick="hapus('.$model->id.')" title="Hapus Email" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>';
            })
            ->make(true);
    }
    public function Margin()
    {
        $query = Margin::all();
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="ubah('.$model->id.')" title="ubah Margin" class="btn btn-xs btn-warning btn-flat"><i class="fa fa-pencil"></i></button> <button onClick="hapus('.$model->id.')" title="Hapus Margin" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>';
            })
            ->make(true);
    }

    public function Parameter()
    {
        $query = Parameter::all();
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="ubah('.$model->id.')" '
                . 'title="ubah Parameter" class="btn btn-xs btn-warning btn-flat">'
                . '<i class="fa fa-pencil"></i></button> '
                . '<button onClick="hapus('.$model->id.')" '
                . 'title="Hapus Parameter" class="btn btn-xs btn-danger btn-flat">'
                . '<i class="fa fa-trash"></i></button> '
                . '<button onClick="detil('.$model->id.')" '
                . 'title="Set Harga" class="btn btn-xs btn-success btn-flat">'
                . '<i class="fa fa-money"></i></button>';
            })
            ->make(true);
    }   

    public function Paket()
    {
        $query = Paket::all();
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="ubah('.$model->id.')" '
                . 'title="ubah Paket" class="btn btn-xs btn-warning btn-flat">'
                . '<i class="fa fa-pencil"></i></button> '
                . '<button onClick="hapus('.$model->id.')" '
                . 'title="Hapus Paket" class="btn btn-xs btn-danger btn-flat">'
                . '<i class="fa fa-trash"></i></button> '
                . '<button onClick="detil('.$model->id.')" '
                . 'title="Lihat Parameter" class="btn btn-xs btn-info btn-flat">'
                . '<i class="fa fa-eye"></i></button> '
                . '<button onClick="setHarga('.$model->id.')" '
                . 'title="Set Harga" class="btn btn-xs btn-success btn-flat">'
                . '<i class="fa fa-money"></i></button>';
            })
            ->editColumn('wajib', function ($q) {
                return $q->wajib == 1 ? 'Ya' : 'Tidak';
            })
            ->make(true);
    }  

    public function PaketParam($id)
    {
        $query = DB::table('paket_parameter')
            ->join('parameter', 'paket_parameter.parameter_id', '=', 'parameter.id')
            ->where('paket_parameter.paket_id', $id)
            ->select(['paket_parameter.id',
            'paket_parameter.paket_id',
            'paket_parameter.parameter_id',
            'parameter.name']);

        
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="hapus('.$model->id.')" '
                . 'title="Hapus Parameter" class="btn btn-xs btn-danger btn-flat">'
                . '<i class="fa fa-trash"></i></button> ';
            })
            ->make(true);
    }    
}
