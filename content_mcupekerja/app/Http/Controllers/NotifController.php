<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Notif;
use App\Http\Controllers\Controller;

use DB;
use Validator;

class NotifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('notif/list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('notif/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'email' => 'email|required|unique:notif,email'
        ];

        $messages = [
            'email'  => 'Kolom :attribute harus diisi dengan email valid!',
            'required'  => 'Kolom :attribute harus diisi!',
            'unique'  => 'Kolom :attribute sudah ada di database',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.notif.create')->withInput()->withErrors($validator);
        } else {
            $notif = new Notif();
            $notif->email = trim($request['email']);
            $notif->save();
        }

        return redirect()->route('mst.notif.index')->with('tersimpan', 1);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notif = Notif::findOrFail($id);

        return view('notif/edit', ['notif' => $notif]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'email' => 'email|required|unique:notif,email'
        ];

        $messages = [
            'email'  => 'Kolom :attribute harus diisi dengan email valid!',
            'required'  => 'Kolom :attribute harus diisi!',
            'unique'  => 'Kolom :attribute sudah ada di database',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.notif.edit', $id)->withInput()->withErrors($validator);
        } else {
            $notif = Notif::findOrFail($id);
            $notif->email = trim($request['email']);
            $notif->save();
        }

        return redirect()->route('mst.notif.index')->with('tersimpan', 1);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notif = Notif::findOrFail($id);
        return $notif->delete() ? 'ok' : abort(404);
    }
}
