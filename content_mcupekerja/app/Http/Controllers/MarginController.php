<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Margin;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Validator;

class MarginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('margin.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('margin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'margin' => 'required|integer|between:1,100'
        ];

        $messages = [
            'required'  => 'Margin harus diisi!',
            'integer'  => 'margin harus berupa angka',
            'between'  => 'margin harus berupa angka antara 1 sampai dengan 100',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('tx.create-margin')->withInput()->withErrors($validator);
        } else {
            $margin = new Margin();
            $margin->dari = trim($request['dari']);
            $margin->ke = trim($request['ke']);
            $margin->margin = trim($request['margin']);
            $margin->save();
            $request->session()->flash('statusmargin', 'Tersimpan!');
        }

        return redirect()->route('mst.margin.index')->with('statusmargin', 'Tersimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $margin = Margin::findOrFail($id);
        return view('margin.edit', ['margin'=>$margin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'margin' => 'required|integer|between:1,100'
        ];

        $messages = [
            'required'  => 'Margin harus diisi!',
            'integer'  => 'margin harus berupa angka',
            'between'  => 'margin harus berupa angka antara 1 sampai dengan 100',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('tx.create-margin')->withInput()->withErrors($validator);
        } else {
            $margin = Margin::findOrFail($id);
            $margin->dari = trim($request['dari']);
            $margin->ke = trim($request['ke']);
            $margin->margin = trim($request['margin']);
            $margin->save();
            $request->session()->flash('statusmargin', 'Tersimpan!');
        }

        return redirect()->route('mst.margin.index')->with('statusmargin', 'Tersimpan!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $margin = Margin::findOrFail($id);
        return $margin->delete() ? 'ok' : abort(404);
    }
}
