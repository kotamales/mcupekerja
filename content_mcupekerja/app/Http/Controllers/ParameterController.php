<?php

namespace App\Http\Controllers;

use App\Harga;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Parameter;
use App\Paket;
use DB;
use Illuminate\Http\Request;
use Validator;

class ParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('parameter/list');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('parameter/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:parameter,name',
            'minimal' => 'required|numeric'
        ];

        $messages = [
            'name.required'  => 'Kolom nama parameter harus diisi!',
            'minimal.required'  => 'Kolom minimal harus diisi!',
            'unique'  => 'Nama parameter yang sama sudah terdaftar di database',
            'numeric'  => 'Kolom :attribute harus berupa angka'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.parameter.create')->withInput()->withErrors($validator);
        } else {
            $parameter = new Parameter();
            $parameter->name = trim($request['name']);
            $parameter->minimal = trim($request['minimal']);
            $parameter->save();
            $this->syncdata();
        }

        return redirect()->route('mst.parameter.index')->with('tersimpan', 1);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parameter = Parameter::findOrFail($id);

        return view('parameter/edit', ['parameter' => $parameter]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:parameter,name',
            'minimal' => 'required|numeric'
        ];

        $messages = [
            'name.required'  => 'Kolom nama parameter harus diisi!',
            'minimal.required'  => 'Kolom minimal harus diisi!',
            'unique'  => 'Nama parameter yang sama sudah terdaftar di database',
            'numeric'  => 'Kolom :attribute harus berupa angka'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.parameter.edit', $id)->withInput()->withErrors($validator);
        } else {
            $parameter = Parameter::findOrFail($id);
            $parameter->name = trim($request['name']);
            $parameter->minimal = trim($request['minimal']);
            $parameter->save();
        }

        return redirect()->route('mst.parameter.index')->with('tersimpan', 1);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $parameter = Parameter::findOrFail($id);

        if (count($parameter->paket) > 0) 
            return 'gagal';

        return $parameter->delete() ? 'ok' : abort(404);
        
    }

    public function price($id)
    {
        $parameter = Parameter::findOrFail($id);

        if (! $parameter) { return redirect()->route('mst.parameter.index');}

        $data = DB::select('SELECT
                    harga_parameter.id,
                    kota.`name`,
                    parameter.id AS parameter_id,
                    harga_parameter.harga1,
                    harga_parameter.harga2,
                    harga_parameter.harga3,
                    kota.id as kota_id
                FROM
                parameter
                INNER JOIN harga_parameter ON harga_parameter.parameter_id = parameter.id
                INNER JOIN kota ON kota.id = harga_parameter.kota_id
                    where parameter.id = ?', [$parameter->id]);
        return view('parameter/price', ['data'=>$data, 'parameter' => $parameter]);
    }

    public function saveprice(Request $request)
    {
        $rules = [
            'f.*.harga1' => 'between:10,9999999999.99',
            'f.*.harga2' => 'between:10,9999999999.99',
            'f.*.harga3' => 'between:10,9999999999.99',
        ];

        $messages = [
            'between' => 'Kolom :attribute harus diantara :min - :max.',
            'unique'  => 'Kolom :attribute sudah ada di database',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.parameter.price')->withInput()->withErrors($validator);
        } else {
            $dt = $request->input('f');
            foreach ($dt as $v) {

                if ($v['id'] == '') {
                    Harga::insert($v);
                } else {
                    Harga::findOrFail($v['id'])->update($v);
                }
            }
        }

        return redirect()->route('mst.parameter.index')->with('tersimpan', 1);
        
    }

    protected function syncdata()
    {
        DB::statement('INSERT IGNORE into harga_parameter(harga_parameter.kota_id, harga_parameter.parameter_id) 
                SELECT
                kota.id,
                parameter.id
                FROM
                kota ,
                parameter');
    }
}
