<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Kode;
use App\Kota;
use App\Lokasi;
use App\Paket;
use App\Parameter;
use App\Registrasi;
use App\User;
use Carbon\Carbon;
use DB;
use Datatables;
use Excel;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;
use Validator;

class FrontController extends Controller
{
    public function Index()
    {
        return view('home');
    }

    public function Register()
    {
        $paket = Paket::with('parameter')->get();
        $kota = Kota::all();
// return $kota;
        return view('register', ['paket'=>$paket, 'kota'=>$kota]);
    }

    public function MitraKami()
    {
        return view('mitra_kami');
    }

    public function TentangKami()
    {
        return view('tentang_kami');
    }

    public function PostRegister(Request $request){
        // echo "<pre>";
        // print_r($request->all());
        // die;
        $rules = [
            'nama_perusahaan' => 'required',
            'total_pekerja' => 'required|numeric|min:75',
            'jenis_usaha' => 'required',
            'alamat' => 'required',
            'pic' => 'required',
            'telepon' => 'required|numeric',
            'email' => 'required|email'
        ];

        $messages = [
            'required' => 'Kolom :attribute harus diisi!',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('register')->withInput()->withErrors($validator);
        } else {
            $register = new Registrasi();
            $register->nama_perusahaan = trim($request['nama_perusahaan']);
            $register->total_pekerja = trim($request['total_pekerja']);
            $register->jenis_usaha = trim($request['jenis_usaha']);
            $register->alamat = trim($request['alamat']);
            $register->pic = trim($request['pic']);
            $register->telepon = trim($request['telepon']);
            $register->email = trim($request['email']);
            $register->total_peserta = trim($request['sum_peserta']);


            if(trim($request['kode_voucher']) != ''){
                $register->kode_voucher = trim($request['kode_voucher']);
            }

            if(trim($request['cashback']) != '')
                $register->cashback = trim($request['cashback']);

            $register->save();

            $id_registrasi = $register->id_registrasi;

            //entri lokasi
            $lokasi = $request['k'];
            $paketnew = $request['paketnew'];
            foreach ($lokasi as $value) {
                $value['registrasi_id'] = $id_registrasi;
                $value['tanggal'] = date('Y-m-d', strtotime($value['tanggal']));
                DB::table('registrasi_kota')->insert($value);
            }
            foreach ($paketnew as $value) {
                $d['registrasi_id'] = $id_registrasi;
                $d['paket_id'] = $value;
                DB::table('registrasi_paket')->insert($d);
            }

        }
        session(['rid' => $id_registrasi]);
        return redirect()->route('thanks');
    }

    public function CekVoucher($kode){
        $query = Kode::where('kode', $kode)->first();
        // return $query;
        if(!$query)
            return response()->json(['sukses' => 0, 'data' => $query]);
        else{
            $valid = date('Y-m-d', strtotime($query->valid_sampai));
            $today = date('Y-m-d');
            if($today < $valid)
                return response()->json(['sukses' => 1, 'data' => $query, 'harga' => number_format($query->harga, 2, ',', '.')]);
            else
                return response()->json(['sukses' => 0, 'data' => $query]);
        }

    }

    //ADMIN PANELS
    public function AdminHome(){
        $chart['thn'] = count(Registrasi::all());
        $chart['bln'] = count(Registrasi::where('created_at', '>=', Carbon::now()->startOfMonth())->get());
        $chart['minggu'] = count(Registrasi::where('created_at', '>=', Carbon::now()->startOfWeek())->get());


        return view('home_admin', $chart);
    }

    public function KelolaUser(){
        return view('kelola_user');
    }

    public function thanks(Request $request){
        
        // ambil registrasi
        // $reg = Registrasi::findOrFail(session('rid'));
        $reg = Registrasi::findOrFail(23);
        return view('thanks', ['reg'=>$reg]);       
    }

    public function KelolaKode(){
        return view('kelola_kode');
    }

    public function FormUser(){
        return view('form_user');
    }

    public function ListKode(){
        $query = Kode::all();
        return Datatables::of($query)
            ->editColumn('harga', function($model){
                return number_format($model->harga, 2, ',', '.');
            })
            ->addColumn('action', function ($model) {
                return '<button onClick="hapus('.$model->id_voucher.')" title="Hapus User" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>';
            })
            ->make(true);
    }

    public function ListUser(){
        $query = User::all();
        return Datatables::of($query)
            ->addColumn('action', function ($model) {
                return '<button onClick="hapus('.$model->id_user.')" title="Hapus User" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button>';
            })
            ->make(true);
    }

    public function SaveUser(Request $request){
        $rules = [
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'password_confirmation' => 'same:password',
        ];

        $messages = [
            'required'  => 'Kolom :attribute harus diisi!',
            'same'      => 'Konfirmasi Password tidak sama',
            'unique'    => 'Username sudah digunakan'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('form-user')->withInput()->withErrors($validator);
        } else {
            $register = new User();
            $register->name = trim($request['name']);
            $register->username = trim($request['username']);
            $register->password = Hash::Make(trim($request['password']));

            $register->save();
        }

        return redirect()->route('kelola-user')->with('registered', 1);
    }

    public function SaveKode(Request $request){
        $rules = [
            'kode' => 'required|unique:kode_voucher,kode',
            'harga' => 'required|numeric',
            'valid_mulai' => 'required',
            'valid_sampai' => 'required'
        ];

        $messages = [
            'unique'    => 'Kode Voucher sudah digunakan!',
            'required'  => 'Kolom :attribute harus diisi!'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('form-user')->withInput()->withErrors($validator);
        } else {
            $register = new Kode();
            $register->kode = trim($request['kode']);
            $register->harga = trim($request['harga']);
            $register->valid_mulai = date('Y-m-d', strtotime($request['valid_mulai']));
            $register->valid_sampai = date('Y-m-d', strtotime($request['valid_sampai']));

            $register->save();
        }

        return redirect()->route('kelola-kode')->with('registered', 1);
    }

    public function DeleteKode(Request $request){
        Kode::find($request['id_voucher'])->delete();
        return response()->json(['sukses' => 1]);
    }

    public function DeleteUser(Request $request){
        User::find($request['id_user'])->delete();
        return response()->json(['sukses' => 1]);
    }

    public function LaporanRegistrasi(){
        return view('list_registrasi');
    }

    public function ListRegistrasi(){
        $query = Registrasi::all();
        return Datatables::of($query)
            ->editColumn('created_at', function($model){
                return date('d-m-Y H:i:s', strtotime($model->created_at));
            })
            ->editColumn('harga', function($model){
                return number_format($model->harga,0);
            })
            ->editColumn('total_peserta', function($model){
                return number_format($model->total_peserta,0);
            })
            ->addColumn('action', function ($model) {
                return '<button onClick="detil('.$model->id_registrasi.')" title="Detil" class="btn btn-xs btn-info btn-flat"><i class="fa fa-list"></i></button> <button onClick="hapus('.$model->id_registrasi.')" title="Hapus" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-trash"></i></button> <button onClick="pdf(\''.$model->file.'\')" title="Lihat PDF" class="btn btn-xs btn-success btn-flat"><i class="fa fa-file-pdf-o"></i></button>';
            })
            ->make(true);
    }

    public function DetilRegistrasi($id){
        $query = Registrasi::find($id);
        // $lokasi = Lokasi::select('alamat', 'jml_peserta', 'pic_lokasi', \DB::raw('DATE_FORMAT(tgl_pemeriksaan, "%d-%m-%Y") as tgl'), 'jam_pemeriksaan')->where('registrasi_id', $id)->get();
        $lokasi = $query->kota;
        $formatted['cashback'] = number_format($query->cashback, 0);
        $formatted['harga'] = number_format($query->harga, 0);
        $formatted['total_peserta'] = number_format($query->total_peserta, 0);
        return Response::json(['data' => $query, 'lok' => $lokasi, 'cashback' => $formatted]);
        // return Response::json(['data' => $query, 'cashback' => $formatted]);
    }

    public function DeleteRegistrasi(Request $request){
        Registrasi::find($request['id_registrasi'])->delete();
        Lokasi::where('registrasi_id', $request['id_registrasi'])->delete();
        DB::table('registrasi_paket')->where('registrasi_id', $request['id_registrasi'])->delete();
        DB::table('registrasi_detail')->where('registrasi_id', $request['id_registrasi'])->delete();
        DB::table('registrasi_kota')->where('registrasi_id', $request['id_registrasi'])->delete();
        return response()->json(['sukses' => 1]);
    }

    public function ExportExcel($word = NULL){
        if(trim($word) != '' || $word != NULL){
            $data = Registrasi::select('created_at as TanggalRegistrasi', 'nama_perusahaan as NamaPerusahaan', 'total_peserta as TotalPeserta', 'jenis_usaha as JenisUsaha', 'alamat as Alamat', 'pic as PIC', 'telepon as Telepon', 'email as Email', 'paket as Paket', 'kode_voucher as KodeVoucher', 'cashback as cashback', 'cashback_type as TipeCashback', 'margin as margin', 'harga as TotalHarga')
                        ->where('nama_perusahaan', 'like', '%'.$word.'%')
                        ->orWhere('total_peserta', 'like', '%'.$word.'%')
                        ->orWhere('jenis_usaha', 'like', '%'.$word.'%')
                        ->orWhere('alamat', 'like', '%'.$word.'%')
                        ->orWhere('pic', 'like', '%'.$word.'%')
                        ->orWhere('email', 'like', '%'.$word.'%')
                        ->orWhere('paket', 'like', '%'.$word.'%')
                        ->orWhere('telepon', 'like', '%'.$word.'%')
                        ->orWhere('kode_voucher', 'like', '%'.$word.'%')
                        ->orWhere('harga_voucher', 'like', '%'.$word.'%')->get();
            $data2 = DB::table('registrasi_kota')
                    ->join('kota', 'registrasi_kota.kota_id', '=', 'kota.id')
                    ->join('registrasi', 'registrasi_kota.registrasi_id', '=', 'registrasi.id_registrasi')
                        ->where('registrasi.nama_perusahaan', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.total_peserta', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.jenis_usaha', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.alamat', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.pic', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.email', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.paket', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.telepon', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.kode_voucher', 'like', '%'.$word.'%')
                        ->orWhere('registrasi.harga_voucher', 'like', '%'.$word.'%')
                        ->select('registrasi.id_registrasi as ID',
                            'kota.name as Kota',
                            'registrasi_kota.pic as PicLokasi',
                            'registrasi_kota.alamat as AlamatLokasi',
                            'registrasi_kota.tanggal as TanggalPemeriksaan',
                            'registrasi_kota.jam as JamPemeriksaan',
                            'registrasi_kota.jml as JumlahPeserta',
                            'registrasi.nama_perusahaan as NamaPerusahaan',
                            'registrasi.alamat as AlamatPerusahaan',
                            'registrasi.pic as PicPerusahaan',
                            'registrasi.telepon as TeleponPerusahaan',
                            'registrasi.email as EmailPerusahaan',
                            'registrasi.paket as PaketYangDipilih')
                        ->get();
            // $data2 = json_decode(json_encode($data2), true);                        
        }
        else{
            $data = Registrasi::select('id_registrasi as ID', 'created_at as TanggalRegistrasi', 'nama_perusahaan as NamaPerusahaan', 'total_peserta as TotalPeserta', 'jenis_usaha as JenisUsaha', 'alamat as Alamat', 'pic as PIC', 'telepon as Telepon', 'email as Email', 'paket as Paket', 'kode_voucher as KodeVoucher', 'cashback as cashback','cashback as cashback', 'cashback_type as TipeCashback', 'margin as margin', 'harga as TotalHarga')->get();
            $data2 = DB::table('registrasi_kota')
                    ->join('kota', 'registrasi_kota.kota_id', '=', 'kota.id')
                    ->join('registrasi', 'registrasi_kota.registrasi_id', '=', 'registrasi.id_registrasi')
                    ->select('registrasi.id_registrasi as REGISTRASI_ID',
                        'kota.name as Kota',
                        'registrasi_kota.pic as PicLokasi',
                        'registrasi_kota.alamat as AlamatLokasi',
                        'registrasi_kota.tanggal as TanggalPemeriksaan',
                        'registrasi_kota.jam as JamPemeriksaan',
                        'registrasi_kota.jml as JumlahPeserta',
                        'registrasi.nama_perusahaan as NamaPerusahaan',
                        'registrasi.alamat as AlamatPerusahaan',
                        'registrasi.pic as PicPerusahaan',
                        'registrasi.telepon as TeleponPerusahaan',
                        'registrasi.email as EmailPerusahaan',
                        'registrasi.paket as PaketYangDipilih')
                    ->orderBy('registrasi.id_registrasi', 'asc')
                    ->get();
            // $data2 = json_decode(json_encode($data2), true);
        }
        $data2 = json_decode(json_encode($data2), true);
        $data = json_decode(json_encode($data), true);

        // echo "<pre>";
        // print_r($data);
        // print_r($data2);
        // exit;

        $data = array_map(function($x){
            $x['margin'] = $x['margin'] . '%';
            $x['TotalPeserta'] = number_format($x['TotalPeserta'],0);
            $x['cashback'] = number_format($x['cashback'],0);
            $x['TotalHarga'] = number_format($x['TotalHarga'],0);
            $x['Paket'] = strip_tags($x['Paket']);
            return $x;
        }, $data);

        $data2 = array_map(function($x){
            $x['JumlahPeserta'] = number_format($x['JumlahPeserta'],0);
            $x['PaketYangDipilih'] = strip_tags($x['PaketYangDipilih']);
            return $x;
        }, $data2);

        Excel::create('Data Registrasi', function($excel) use ($data, $data2){
            $excel->sheet('Data Registrasi', function($sheet) use($data){
                $sheet->fromArray($data);
            });
            $excel->sheet('Data Lokasi', function($sheet) use($data2){
                $sheet->fromArray($data2);
            });
        })->download('xlsx');
    }


    public function makepdf($html = '', $id = 0)
    {
        
        $namafile = 'report/sip'. rand() .'.pdf'; 
        try {
            // ob_start();
            // include dirname(__FILE__).'/res/exemple00.php';
            $content = view('pdf')->render();
            $html2pdf = new Html2Pdf('P', 'A4', 'fr');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content);

            $pdf= $html2pdf->Output($namafile, 'F');

            // Storage::put($namafile, $pdf);

            
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }
}
