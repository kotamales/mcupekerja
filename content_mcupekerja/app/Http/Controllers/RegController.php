<?php

namespace App\Http\Controllers;

use App\Helpers\Tanggal;
use App\Http\Controllers\Controller;
use App\Kode;
use App\Kota;
use App\Lokasi;
use App\Margin;
use App\Notif;
use App\Paket;
use App\Parameter;
use App\Registrasi;
use App\User;
use Carbon\Carbon;
use DB;
use Datatables;
use Excel;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Mail;
use Response;
use Session;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Html2Pdf;
use Validator;

class RegController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paket = Paket::with('parameter')->get();
        $parameter = Parameter::all();
        $kota = Kota::all();

        // echo $paket;die;
        return view('registrasi', ['paket'=>$paket, 'parameter'=>$parameter, 'kota'=>$kota]);
    }

    public function store(Request $request)
    {
        $rules = [
            'nama_perusahaan' => 'required',
            'jenis_usaha' => 'required',
            'alamat' => 'required',
            'pic' => 'required',
            'telepon' => 'required|numeric',
            'email' => 'required|email',
            'kota' => 'required'
        ];

        $messages = [
            'required' => 'Kolom :attribute harus diisi!',
            'email.email' => 'Email yang anda masukkan tidak valid!',
            'kota.required'  => 'Anda belum memilih lokasi !',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            // var_dump ($request->all());
            // die();
            return redirect()->route('register')->withInput()->withErrors($validator);
        } else {
            $register = new Registrasi();
            $register->nama_perusahaan = trim($request['nama_perusahaan']);
            $register->jenis_usaha = trim($request['jenis_usaha']);
            $register->alamat = trim($request['alamat']);
            $register->pic = trim($request['pic']);
            $register->telepon = trim($request['telepon']);
            $register->email = trim($request['email']);
            $register->total_peserta = trim($request['sum_peserta']);


            if(trim($request['kode_voucher']) != ''){
                $register->kode_voucher = trim($request['kode_voucher']);
            }

            if(trim($request['cashback']) != '')
                $register->cashback = trim($request['cashback']);

            if(trim($request['cashback_type']) != '') {
                $register->cashback_type = ($request['cashback_type'] == 0) ? 'Per Quotation' : 'Per Orang';
            }
            $margin = Margin::findOrFail(1);
            $register->margin = $margin->margin;

            $register->save();

            $id_registrasi = $register->id_registrasi;

            //entri lokasi
            $lokasi = $request['kota'];
            $paketnew = $request['pivot'];
            foreach ($lokasi as $value) {
                unset($value['kotane']);
                $value['registrasi_id'] = $id_registrasi;
                $value['tanggal'] = date('Y-m-d', strtotime($value['tanggal']));
                $value['jam'] = '09:00:00';
                DB::table('registrasi_kota')->insert($value);
            }
            foreach ($paketnew as $value) {
                $d['registrasi_id'] = $id_registrasi;
                $d['pivot_id'] = $value;
                DB::table('registrasi_paket')->insert($d);
            }

            $this->make_detail($id_registrasi);
            $this->makepdf($id_registrasi);



        }

        session(['rid' => $id_registrasi]);
        return redirect()->route('thanks');
    }

    public function thanks(Request $request){
        
        $id = Session::get('rid');
        $reg = Registrasi::findOrFail($id);
        return view('thanks', ['reg'=>$reg]);       
    }

    public function make_detail($id){
        
        $reg = Registrasi::findOrFail($id);
        $margin = Margin::findOrFail(1);
        $regs = Registrasi::findOrFail($id)->toArray();

        $detail = DB::select("SELECT
            registrasi_paket.registrasi_id,
            paket.`name` as paket,
            GROUP_CONCAT(parameter.`name` ORDER BY parameter.id ASC SEPARATOR ', ') as param,
            registrasi_kota.kota_id,
            sum(harga_parameter.harga1) as aharga1,
            SUM(harga_parameter.harga2) as aharga2,
            SUM(harga_parameter.harga3) as aharga3,
            registrasi_kota.jml,
            registrasi_kota.id as registrasikota_id
            FROM
            registrasi_paket
            INNER JOIN paket_parameter ON registrasi_paket.pivot_id = paket_parameter.id
            INNER JOIN paket ON paket.id = paket_parameter.paket_id
            INNER JOIN parameter ON parameter.id = paket_parameter.parameter_id
            INNER JOIN registrasi_kota ON registrasi_kota.registrasi_id = registrasi_paket.registrasi_id
            INNER JOIN harga_parameter ON harga_parameter.kota_id = registrasi_kota.kota_id AND harga_parameter.parameter_id = parameter.id
            INNER JOIN kota ON kota.id = registrasi_kota.kota_id
            WHERE registrasi_paket.registrasi_id = :id
            GROUP BY registrasi_paket.registrasi_id, paket_parameter.paket_id, kota.id, registrasi_kota.id", ['id'=>$id]);

        $pakets = DB::select("SELECT
            paket.`name`,
            GROUP_CONCAT(parameter.`name` SEPARATOR ', ') as param
            FROM
            registrasi_paket
            INNER JOIN paket_parameter ON registrasi_paket.pivot_id = paket_parameter.id
            INNER JOIN parameter ON parameter.id = paket_parameter.parameter_id
            INNER JOIN paket ON paket.id = paket_parameter.paket_id
            WHERE registrasi_paket.registrasi_id = :id
            GROUP BY paket.`name`", ['id'=>$id]);

        $kotas = DB::select("SELECT
                sum(registrasi_kota.jml) as nilai
                FROM
                registrasi_kota
                where registrasi_id = :id", ['id'=>$id]);

            $jmlparam =  DB::table('registrasi_paket')->where('registrasi_id', '=', $id)->count();
            if (count($detail) > 0 ) {
                $detail = array_map(function($object) use ($reg, $margin, $jmlparam, $pakets, $kotas) {
                    if ($reg->cashback > 0 && $reg->cashback_type == 'Per Quotation') {
                        $object->harga1 = round($object->aharga1 + ($object->aharga1 * ($margin->margin /100)) + ($object->aharga1 / ($jmlparam * $reg->total_peserta)));
                        $object->harga2 = round($object->aharga2 + ($object->aharga2 * ($margin->margin /100)) + ($object->aharga2 / ($jmlparam * $reg->total_peserta)));
                        $object->harga3 = round($object->aharga3 + ($object->aharga3 * ($margin->margin /100)) + ($object->aharga3 / ($jmlparam * $reg->total_peserta)));
                    } else if ($reg->cashback > 0 && $reg->cashback_type == 'Per Orang') {
                        $object->harga1 = $object->aharga1 + $reg->cashback; 
                        $object->harga2 = $object->aharga2 + $reg->cashback; 
                        $object->harga3 = $object->aharga3 + $reg->cashback; 

                    }else {
                        $object->harga1 = $object->aharga1; 
                        $object->harga2 = $object->aharga2; 
                        $object->harga3 = $object->aharga3; 
                    } 
                    return (array) $object;
                }, $detail);

                $dpaket = '<ol style=\'-webkit-padding-start: 15px;\'>';
                foreach ($detail as $value) {
                    DB::table('registrasi_detail')->insert($value);      
                }
                foreach ($pakets as $value) {
                    $dpaket .= '<li>';
                    $dpaket .= $value->name . ' ( '. $value->param .' ) ';
                    $dpaket .= '</li>';   
                }
                $dpaket .= '</ol>';
                $reg->paket = $dpaket;
                

                $reg->total_peserta = $kotas[0]->nilai;

                $reg->save();
            }

            


            $notif = Notif::all();

            Mail::send('emails.welcome', $regs, function ($message) use ($notif) {
                $message->from('mcu.dont.reply@gmail.com', 'MCU System Notification');

                foreach ($notif as $k => $v) {
                    if ($k == 0 ) {
                        $message->to($v->email);
                    } else {
                        $message->cc($v->email);
                    }
                }
            });
    }

    public function genpdf()
    {
        return 'wkwkkwkw';
        // exit;
        // ob_implicit_flush(true);
        // echo "Processing ... <br/>"; // Or give out JSON output
        // $pdf = Registrasi::where('id_registrasi', '>', 20)->get();
        // foreach ($pdf as $v) {
        //     echo "Processing ID ". $v->id_registrasi . "... <br/>"; // Or give out JSON output
        //     ob_flush();
        //     // $this->make_detail($v->id_registrasi);
        //     $this->makepdf($v->id_registrasi);
            
        //     echo "ID ". $v->id_registrasi . "Done ... <br/>"; // Or give out JSON output
        //     ob_flush();
        //     sleep(1); //A time-consuming synchronous process (SMTP mail, maybe?)
        // }
        // echo "Done";
    }



    public function makepdf($id =33)
    {
        $newid = sprintf('%05d', $id);
        $namafile = 'report/r-'. $newid .'.pdf';

        $dt = date('Y-m-d H:i:s');

        $dte = date('Y-m-d H:i:s', strtotime('+1 month'));
        
        $tgl = Tanggal::keIndonesia($dt);
        $tgl2 = Tanggal::keIndonesia($dte);

        if (file_exists($namafile)) {
            unlink($namafile);
        }

        try {
            $data['tanggal'] = $tgl;
            $data['tanggal2'] = $tgl2;
            $data['reg'] = Registrasi::findOrFail($id);
            $data['no'] = 'R-' . $newid;
            $detailx = DB::select("SELECT
                kota.`name` AS kota,
                registrasi_detail.id,
                registrasi_detail.registrasi_id,
                registrasi_detail.kota_id,
                registrasi_detail.paket,
                registrasi_detail.param,
                registrasi_detail.harga1,
                registrasi_detail.harga2,
                registrasi_detail.harga3,
                registrasi_detail.jml,
                DATE_FORMAT(registrasi_kota.tanggal, \"%d %M %Y\") as tanggal,
                registrasi_kota.tanggal as tanggal2,
                registrasi_kota.jam,
                registrasi_kota.alamat,
                registrasi_detail.registrasikota_id
                FROM
                registrasi_detail
                INNER JOIN kota ON kota.id = registrasi_detail.kota_id
                INNER JOIN registrasi_kota ON registrasi_kota.registrasi_id = registrasi_detail.registrasi_id AND registrasi_kota.kota_id = registrasi_detail.kota_id
                    AND registrasi_kota.id = registrasi_detail.registrasikota_id
                    WHERE registrasi_detail.registrasi_id = :id", ['id'=>$id]);
            $detail = array_map(function($x){
                $x->alkot = $x->kota . str_slug($x->alamat, "_"). $x->registrasikota_id;
                $x->t1 = $x->jml * $x->harga1;
                $x->tanggal2 = $this->nm_hari($x->tanggal2);
                $x->t2 = $x->jml * $x->harga2;
                $x->t3 = $x->jml * $x->harga3;
                return $x;
            }, $detailx);
            $details = $this->array_group_by($detail,'alkot');
            $data['detail'] = $details;
            $data['harga1'] = array_sum(array_pluck($detailx, 't1'));
            $data['harga2'] = array_sum(array_pluck($detailx, 't2'));
            $data['harga3'] = array_sum(array_pluck($detailx, 't3'));
            // echo "<pre>";
            // print_r($details);
            // die;
            $content = view('pdf',$data)->render();
            $html2pdf = new Html2Pdf('P', 'A4', 'fr');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($content);

            $pdf= $html2pdf->Output($namafile, 'F');

            if ($data['reg']->total_peserta <= 100 ) {
                $data['reg']->harga = $data['harga1'];
            }
            elseif ($data['reg']->total_peserta > 100 AND $data['reg']->total_peserta < 300) {
                $data['reg']->harga = $data['harga2'];
            }
            else {
                $data['reg']->harga = $data['harga3'];
            }

            $data['reg']->file = $namafile;
            $data['reg']->save();
            // Storage::put($namafile, $pdf);

            
        } catch (Html2PdfException $e) {
            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }

    function array_group_by(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
            trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }
        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;
            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && isset($value->{$_key})) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }
            if ($key === null) {
                continue;
            }
            $grouped[$key][] = $value;
        }
        // Recursively build a nested grouping if more parameters are supplied
        // Each grouped array value is grouped according to the next sequential key
        if (func_num_args() > 2) {
            $args = func_get_args();
            foreach ($grouped as $key => $value) {
                $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('array_group_by', $params);
            }
        }
        return $grouped;
    }

    public function nm_hari($tanggal='')
    {
        $hari   = date('l', microtime($tanggal));
        $hari_indonesia = array('Monday'  => 'Senin',
           'Tuesday'  => 'Selasa',
           'Wednesday' => 'Rabu',
           'Thursday' => 'Kamis',
           'Friday' => 'Jumat',
           'Saturday' => 'Sabtu',
           'Sunday' => 'Minggu');
        return $hari_indonesia[$hari];
    }

}
