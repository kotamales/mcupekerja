<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Parameter;
use App\Paket;
use App\Margin;
use DB;
use Illuminate\Http\Request;
use Validator;

class TxController extends Controller
{
    public function param($id)
    {
        $paket = Paket::findOrFail($id);

        return view('tx/list', $paket);
    }

    public function margin()
    {
        return view('tx/margin_list');
    }

    public function create_margin()
    {
        return view('tx/margin');
    }

    public function create($id)
    {
        $list = DB::select('SELECT
            parameter.id,
            parameter.`name`
            FROM
            parameter
            WHERE id NOT in (SELECT
            paket_parameter.parameter_id
            FROM
            paket_parameter
            WHERE paket_parameter.paket_id = ?)', [$id]);

        if (count($list)  > 0 ) {
            return view('tx/create', ['id'=>$id,'list'=>$list]);
        }

         return redirect()->route('tx.param', $id)->with('tersimpan', 0);   
    }

    public function del($id)
    {
        DB::table('paket_parameter')->where('id', '=', $id)->delete();
        return 'ok';
    }

    public function save(Request $request, $id)
    {
        $paket = trim($request['param']);
        
        DB::table('paket_parameter')->insert(
            ['paket_id' => $id, 'parameter_id' => $paket]
        );

        return redirect()->route('tx.param', $id)->with('tersimpan', 1);
    }

    public function savemargin(Request $request)
    {
        $rules = [
            'margin' => 'required|integer|between:1,100'
        ];

        $messages = [
            'required'  => 'Margin harus diisi!',
            'integer'  => 'margin harus berupa angka',
            'between'  => 'margin harus berupa angka antara 1 sampai dengan 100',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('tx.create-margin')->withInput()->withErrors($validator);
        } else {
            $margin = new Margin();
            $margin->dari = trim($request['dari']);
            $margin->ke = trim($request['ke']);
            $margin->margin = trim($request['margin']);
            $margin->save();
            $request->session()->flash('statusmargin', 'Tersimpan!');
        }

        return redirect()->route('tx.margin')->with('statusmargin', 'Tersimpan!');
    }


}
