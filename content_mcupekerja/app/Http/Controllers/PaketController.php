<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Paket;
use App\PaketHarga;
use App\Http\Controllers\Controller;

use DB;
use Validator;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('paket/list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paket/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:paket,name',
            'minimal' => 'numeric',
            // 'biaya2' => 'numeric',
            // 'biaya3' => 'numeric',
            // 'diskon1' => 'numeric',
            // 'diskon2' => 'numeric',
            // 'diskon3' => 'numeric',
        ];

        $messages = [
            'required'  => 'Kolom nama paket harus diisi!',
            'unique'  => 'Nama Paket yang sama sudah terdaftar di database',
            'numeric'  => 'Kolom :attribute harus berupa angka',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.paket.create')->withInput()->withErrors($validator);
        } else {
            $paket = new Paket();
            $paket->name = trim($request['name']);
            $paket->wajib = $request->has(['wajib']) ? 1 : 0;
            $paket->minimal = trim($request['minimal']);
            // $paket->biaya1 = $request['biaya1'];
            // $paket->biaya2 = $request['biaya2'];
            // $paket->biaya3 = $request['biaya3'];
            // $paket->diskon1 = $request['diskon1'];
            // $paket->diskon2 = $request['diskon2'];
            // $paket->diskon3 = $request['diskon3'];
            $paket->save();
            $this->syncdata();
        }

        return redirect()->route('mst.paket.index')->with('tersimpan', 1);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paket = Paket::findOrFail($id);
        if ($paket->wajib == 1) {
            $paket->checked = 'checked';
        } else {
            $paket->checked = '';
        }
        return view('paket/edit', ['paket' => $paket]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:paket,name,' . $id,
            'minimal' => 'numeric',
            // 'biaya2' => 'numeric',
            // 'biaya3' => 'numeric',
            // 'diskon1' => 'numeric',
            // 'diskon2' => 'numeric',
            // 'diskon3' => 'numeric',
        ];

        $messages = [
            'required'  => 'Kolom nama paket harus diisi!',
            'unique'  => 'Nama Paket yang sama sudah terdaftar di database',
            'numeric'  => 'Kolom :attribute harus berupa angka',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.paket.edit', $id)->withInput()->withErrors($validator);
        } else {
            $paket = Paket::findOrFail($id);
            $paket->name = trim($request['name']);
            $paket->minimal = trim($request['minimal']);
            $paket->wajib = $request->has(['wajib']) ? 1 : 0;
            // $paket->biaya1 = $request['biaya1'];
            // $paket->biaya2 = $request['biaya2'];
            // $paket->biaya3 = $request['biaya3'];
            // $paket->diskon1 = $request['diskon1'];
            // $paket->diskon2 = $request['diskon2'];
            // $paket->diskon3 = $request['diskon3'];
            $paket->save();
        }

        return redirect()->route('mst.paket.index')->with('tersimpan', 1); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paket = Paket::findOrFail($id);
        return $paket->delete() ? 'ok' : abort(404);
    }

    public function price($id)
    {
        $paket = Paket::findOrFail($id);

        if (! $paket) { return redirect()->route('mst.paket.index');}

        $data = DB::select('SELECT
            harga_paket.id,
            kota.`name`,
            paket.id AS paket_id,
            harga_paket.harga1,
            harga_paket.harga2,
            harga_paket.harga3,
            kota.id AS kota_id,
            sum(harga_parameter.harga1) as aharga1,
            sum(harga_parameter.harga2) as aharga2,
            sum(harga_parameter.harga3) as aharga3
            FROM
            paket
            INNER JOIN harga_paket ON harga_paket.paket_id = paket.id
            INNER JOIN kota ON kota.id = harga_paket.kota_id
            INNER JOIN paket_parameter ON paket_parameter.paket_id = harga_paket.paket_id
            INNER JOIN harga_parameter ON harga_parameter.kota_id = kota.id AND paket_parameter.parameter_id = harga_parameter.parameter_id
            where paket.id = ?
            GROUP BY harga_paket.id ', [$paket->id]);
        return view('paket/price', ['data'=>$data, 'paket' => $paket]);
    }

    public function saveprice(Request $request)
    {
        $rules = [
            'f.*.harga1' => 'between:10,9999999999.99',
            'f.*.harga2' => 'between:10,9999999999.99',
            'f.*.harga3' => 'between:10,9999999999.99',
        ];

        $messages = [
            'between' => 'Kolom :attribute harus diantara :min - :max.',
            'unique'  => 'Kolom :attribute sudah ada di database',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.paket.price')->withInput()->withErrors($validator);
        } else {
            $dt = $request->input('f');
            foreach ($dt as $v) {

                if ($v['id'] == '') {
                    PaketHarga::insert($v);
                } else {
                    PaketHarga::findOrFail($v['id'])->update($v);
                }
            }
        }

        return redirect()->route('mst.paket.index')->with('tersimpan', 1);
        
    }

    protected function syncdata()
    {
        DB::statement('INSERT IGNORE into harga_paket(harga_paket.kota_id, harga_paket.paket_id) 
                SELECT
                kota.id,
                paket.id
                FROM
                kota ,
                paket');
    }
}
