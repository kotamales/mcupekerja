<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Kota;
use App\Http\Controllers\Controller;

use DB;
use Validator;

class KotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('kota/list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kota/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:kota,name'
        ];

        $messages = [
            'required'  => 'Kolom :attribute harus diisi!',
            'unique'  => 'Kolom :attribute sudah ada di database',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.kota.create')->withInput()->withErrors($validator);
        } else {
            $kota = new Kota();
            $kota->name = trim($request['name']);
            $kota->save();

            $this->syncdata();
        }

        return redirect()->route('mst.kota.index')->with('tersimpan', 1);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kota = Kota::findOrFail($id);

        return view('kota/edit', ['kota' => $kota]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:kota,name,' . $id, 
        ];

        $messages = [
            'required'  => 'Kolom :attribute harus diisi!',
            'unique'  => 'Kolom :attribute sudah ada di database',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('mst.kota.edit', $id)->withInput()->withErrors($validator);
        } else {
            $kota = Kota::findOrFail($id);
            $kota->name = trim($request['name']);
            $kota->save();
        }

        return redirect()->route('mst.kota.index')->with('tersimpan', 1);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kota = Kota::findOrFail($id);
        return $kota->delete() ? 'ok' : abort(404);
        
    }

    protected function syncdata()
    {
        DB::statement('INSERT IGNORE into harga_parameter(harga_parameter.kota_id, harga_parameter.parameter_id) 
                SELECT
                kota.id,
                parameter.id
                FROM
                kota ,
                parameter');
        DB::statement('INSERT IGNORE into harga_paket(harga_paket.kota_id, harga_paket.paket_id) 
                SELECT
                kota.id,
                paket.id
                FROM
                kota ,
                paket');
    }

}
