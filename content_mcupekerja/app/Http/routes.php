<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('home', ['uses' => 'FrontController@Index', 'as' => 'home']);
Route::get('thanks', ['uses' => 'RegController@thanks', 'as' => 'thanks']);
Route::get('mitra-kami', ['uses' => 'FrontController@MitraKami', 'as' => 'mitra-kami']);
Route::get('tentang-kami', ['uses' => 'FrontController@TentangKami', 'as' => 'tentang-kami']);
Route::get('register', ['uses' => 'RegController@index', 'as' => 'register']);
// Route::get('cek-voucher/{kode}', ['uses' => 'FrontController@CekVoucher', 'as' => 'cek-voucher']);
Route::get('pdf', ['uses' => 'RegController@makepdf', 'as' => 'pdf']);
Route::get('gpdf', ['uses' => 'RegController@genpdf', 'as' => 'gpdf']);
Route::post('save-register', ['uses' => 'RegController@store', 'as' => 'save-register']);

Route::get('home-admin', array('middleware' => 'auth', 'uses' => 'FrontController@AdminHome', 'as' => 'home-admin'));
// Route::get('kelola-kode', array('middleware' => 'auth', 'uses' => 'FrontController@KelolaKode', 'as' => 'kelola-kode'));
Route::get('kelola-user', array('middleware' => 'auth', 'uses' => 'FrontController@KelolaUser', 'as' => 'kelola-user'));
Route::get('form-user', array('middleware' => 'auth', 'uses' => 'FrontController@FormUser', 'as' => 'form-user'));
Route::post('save-user', array('middleware' => 'auth', 'uses' => 'FrontController@SaveUser', 'as' => 'save-user'));
// Route::post('save-kode', array('middleware' => 'auth', 'uses' => 'FrontController@SaveKode', 'as' => 'save-kode'));
Route::get('laporan-registrasi', array('middleware' => 'auth', 'uses' => 'FrontController@LaporanRegistrasi', 'as' => 'laporan-registrasi'));
Route::get('data-listuser', array('middleware' => 'auth', 'uses' => 'FrontController@ListUser', 'as' => 'data-listuser'));
Route::get('data-listkode', array('middleware' => 'auth', 'uses' => 'FrontController@ListKode', 'as' => 'data-listkode'));
Route::post('delete-user', array('middleware' => 'auth', 'uses' => 'FrontController@DeleteUser', 'as' => 'delete-user'));
// Route::post('delete-kode', array('middleware' => 'auth', 'uses' => 'FrontController@DeleteKode', 'as' => 'delete-kode'));

Route::post('delete-registrasi', array('middleware' => 'auth', 'uses' => 'FrontController@DeleteRegistrasi', 'as' => 'delete-registrasi'));
Route::get('data-listregistrasi', array('middleware' => 'auth', 'uses' => 'FrontController@ListRegistrasi', 'as' => 'data-listregistrasi'));
Route::get('detil-registrasi/{id}', array('middleware' => 'auth', 'uses' => 'FrontController@DetilRegistrasi', 'as' => 'detil-registrasi'));
Route::get('export-excel/{word?}', array('middleware' => 'auth', 'uses' => 'FrontController@ExportExcel', 'as' => 'export-excel'));

Route::get('auth/login', array('uses' => 'Auth\AuthController@getLogin', 'as' => 'login'));
Route::post('auth/login', array('uses' => 'Auth\AuthController@postLogin', 'as' => 'postLogin'));
// Route::get('auth/register', array('uses' => 'Auth\AuthController@getRegister', 'as' => 'register'));
// Route::post('auth/register', array('uses' => 'Auth\AuthController@postRegister', 'as' => 'postRegister'));
Route::get('auth/logout', array('uses' => 'Auth\AuthController@getLogout', 'as' => 'logout'));

Route::get('/', function () {
    return redirect()->route('home');
});

Route::group(['middleware' => 'auth', 'prefix' => 'mst'], function () {

	Route::resource('kota', 'KotaController',
      ['except' => ['show']]);
	Route::resource('parameter', 'ParameterController',
      ['except' => ['show']]);
	Route::resource('margin', 'MarginController',
      ['except' => ['show']]);
	Route::resource('paket', 'PaketController',
      ['except' => ['show']]);
	Route::resource('notif', 'NotifController',
      ['except' => ['show']]);
	Route::get('parameter/price/{id}', 
		['uses' => 'ParameterController@price', 
		'as' => 'mst.parameter.price']);
	Route::post('parameter/price/', 
		['uses' => 'ParameterController@saveprice', 
		'as' => 'mst.parameter.save']);
	Route::get('paket/price/{id}', 
		['uses' => 'PaketController@price', 
		'as' => 'mst.paket.price']);
	Route::post('paket/price/', 
		['uses' => 'PaketController@saveprice', 
		'as' => 'mst.paket.save']);
});

Route::group(['middleware' => 'auth', 'prefix' => 'src'], function () {
	Route::get('kota', ['uses' => 'SrcController@Kota', 'as' => 'src.kota']);
	Route::get('parameter', ['uses' => 'SrcController@Parameter', 'as' => 'src.parameter']);
	Route::get('paket', ['uses' => 'SrcController@Paket', 'as' => 'src.paket']);
	Route::get('margin', ['uses' => 'SrcController@Margin', 'as' => 'src.margin']);
	Route::get('notif', ['uses' => 'SrcController@Notif', 'as' => 'src.notif']);
	Route::get('paket-param/{id}', ['uses' => 'SrcController@PaketParam', 'as' => 'src.paket-param']);
});

Route::group(['middleware' => 'auth', 'prefix' => 'tx'], function () {
	Route::get('param/{id}', ['uses' => 'TxController@param', 'as' => 'tx.param']);
	Route::get('create/{id}', ['uses' => 'TxController@create', 'as' => 'tx.create']);
	Route::post('save/{id}', ['uses' => 'TxController@save', 'as' => 'tx.save']);
	Route::delete('param/{id}', ['uses' => 'TxController@del', 'as' => 'tx.del']);
	
});
