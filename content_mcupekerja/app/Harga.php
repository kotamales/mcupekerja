<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Harga extends Model
{
  protected $table = 'harga_parameter';
	protected $fillable = ['parameter_id','kota_id','harga1','harga2','harga3'];
}
