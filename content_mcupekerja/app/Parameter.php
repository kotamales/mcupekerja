<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
	protected $table = 'parameter';
	protected $fillable = ['name','minimal'];

	public function paket()
    {
        return $this->belongsToMany('App\Paket');
    }
}
