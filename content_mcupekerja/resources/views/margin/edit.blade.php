@extends('layout_admin')
@section('title')
Data Master Margin - MCU Pekerja Admin Panel
@endsection

@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Margin
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{route('home-admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-money active"></i> Tambah Margin</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Tambah Margin</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
                    {!! Form::open(['route' => ['mst.margin.update', $margin->id], 
                        'class' => 'form-horizontal']) !!}
                        <input type="hidden" name="_method" value="PATCH">
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <label for="dari" class="col-sm-2 control-label">Dari*</label>
                        <div class="col-sm-10">
                          <input value="{{old('dari', $margin->dari)}}" type="number" class="form-control" id="dari" name="dari" placeholder="" required>
                          <p class="help-block" style="color:red">{{$errors->first('dari')}}</p>
                        </div>                      
                      </div>
                      <div class="form-group">
                        <label for="ke" class="col-sm-2 control-label">Ke*</label>
                        <div class="col-sm-10">
                          <input value="{{old('ke', $margin->ke)}}" type="number" class="form-control" id="ke" name="ke" placeholder="" required>
                          <p class="help-block" style="color:red">{{$errors->first('ke')}}</p>
                        </div>                      
                      </div>
                      <div class="form-group">
                        <label for="margin" class="col-sm-2 control-label">Prosentase Margin*</label>
                        <div class="col-sm-10">
                          <input value="{{old('margin', $margin->margin)}}" type="number" class="form-control" id="margin" name="margin" placeholder="" required>
                          <p class="help-block" style="color:red">{{$errors->first('margin')}}</p>
                        </div>                      
                      </div>
                      <button type="submit" class="btn btn-info pull-right">Simpan</button>
                    {!! Form::close() !!}
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
          </div>
          <!-- Main row -->
        </section><!-- /.content -->
@endsection
@section('css')
<style>
  input[type="number"] {
    -moz-appearance: textfield;
}
input[type="number"]:hover,
input[type="number"]:focus {
    -moz-appearance: number-input;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
  margin: 0; /* Removes leftover margin */
}
</style>
<link href="{{asset('assets/admin/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/iCheck/all.css')}}" rel="stylesheet">
@endsection
@section('js')
  <script src="{{asset('assets/admin/plugins/select2/select2.min.js')}}"></script>
  <script src="{{asset('assets/admin/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript">
  
</script>
@endsection