<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{asset('assets/img/logo.jpg')}}" type="image/gif">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('assets/admin/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    @yield('css')
    <link rel="stylesheet" href="{{asset('assets/admin/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{asset('assets/admin/css/skins/_all-skins.min.css')}}">
    <link href="{{asset('assets/admin/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
    
  </head>
  <body class="hold-transition skin-red-light sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>MCU</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>MCU</b>Pekerja</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{asset('assets/admin/img/users.png')}}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{asset('assets/admin/img/users.png')}}" class="img-circle" alt="User Image">
                    <p>
                      {{Auth::user()->name}}
                      <small>{{Auth::user()->username}}</small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <!-- <div class="pull-left">
                      <a href="#" class="btn btn-primary btn-flat">Ubah Password</a>
                    </div> -->
                    <div class="pull-right">
                      <a href="{{\URL::to('auth/logout')}}" class="btn btn-danger btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{asset('assets/admin/img/users.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{Auth::user()->name}}</p>
              <a href="#"><i class="fa fa-circle text-success"></i> {{Auth::user()->username}}</a>
            </div>
          </div>
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MENU UTAMA</li>
            <li><a href="{{route('home-admin')}}"><i class="fa fa-home"></i> <span>Home</span></a></li>
            <li><a href="{{route('laporan-registrasi')}}"><i class="fa fa-archive"></i> <span>Laporan Registrasi</span></a></li>
            <li class="header">MASTER DATA</li>
            <li><a href="{{route('mst.kota.index')}}"><i class="fa fa-building"></i> <span>Master Kota</span></a></li>
            <li><a href="{{route('mst.parameter.index')}}"><i class="fa fa-gear"></i> <span>Master Parameter</span></a></li>
            <li><a href="{{route('mst.paket.index')}}"><i class="fa fa-archive"></i> <span>Master Paket</span></a></li>
            <li class="header">SUPPORT</li>
            <!-- <li><a href="{{--route('kelola-kode')--}}"><i class="fa fa-list"></i> <span>Kode Voucher</span></a></li> -->
            <li><a href="{{route('kelola-user')}}"><i class="fa fa-users"></i> <span>User</span></a></li>
            <li><a href="{{route('mst.notif.index')}}"><i class="fa fa-envelope"></i> <span>Email Notifikasi</span></a></li>
            <li><a href="{{route('mst.margin.index')}}"><i class="fa fa-money"></i> <span>Margin</span></a></li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @yield('content')
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2017 <a target="_blank" href="#">Emcorp Studio</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="{{asset('assets/admin/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('assets/admin/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('assets/admin/js/app.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/sweetalert/sweetalert.min.js')}}"></script>
    @yield('js')
    <script type="text/javascript">
      function isValidDate(date_string)
      {
        var pattern = /^([0-9]{2})\-([0-9]{2})\-([0-9]{4})$/;
        return pattern.test(date_string);
      }
    </script>
  </body>
</html>
