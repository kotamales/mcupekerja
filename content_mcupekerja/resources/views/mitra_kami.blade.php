@extends('layout')
@section('title')
Mitra Kami - MCU Pekerja
@endsection
@section('content')
<div class="container content">
	<div class="headline"><h2>Sejarah Mitra Lab</h2></div>
	<div class="row">
		<div class="col-md-3 text-center md-margin-bottom-40">
			<div class="responsive-video">
				<img src="{{asset('assets/img/kantor_mitralab.jpg')}}">
			</div>
		</div>
		<div class="col-md-9 md-margin-bottom-40">
			<h4 style="text-align: justify;">Berawal dari idealisme untuk dapat memberikan pelayanan pemeriksaan laboratorium yang bermutu dan dapat dipercaya.</h4>
			<p style="text-align: justify;">
				Mitralab berdiri sejak tanggal 22 Juni 2000 di jl.Taman Duta Barat
				A1.No.2. Cisalak - Depok. Dengan berjalannya waktu pengguna
				layanan laboratorium klinik Mitralab bukan hanya masyarakat
				sekitarnya tetapi juga dari perusahaan di sekitar lokasi Mitralab ,
				jl.Raya Bogor dan sekitarnya.. Namun adanya keterbatasan lahan,
				maka sejak tanggal 7 September 2005 lokasi laboratorium klinik 2 MITRALAB Occupational Healthcare
				Mitralab pindah ke jl.Lapangan Tembak Kav . F no.1D Cibubur – Jakarta Timur.
			</p>
			<p>
				Dari pengalaman menangani Medical Check Up karyawan perusahaan
				dari berbagai macam jenis industri , maka diperlukan pengetahuan
				dan penanganan yang khusus .Bersamaan dengan perkembangan
				industri di bagian timur Jakarta , khususnya di kawasan Bekasi,
				Cikarang dan Karawang menyebabkan kami membuka :
			</p>
			<h4>MITRALAB Occupational Healthcare</h4>
			<p>yang khusus menangani pemeriksaan kesehatan bagi karyawan ataupun calon karyawan yang akan bekerja di perusahaan.</p>
		</div>
	</div><!--/row-->
</div>

<div class="container content">
	<div class="headline"><h2>Tentang Kami</h2></div>
	<div class="row">
		<div class="col-md-6 md-margin-bottom-40">
			<p><b>MitraLab</b> memiliki visi yaitu Menjadi laboratorium klinik yang terpilih karena kualitas pemeriksaan dan pelayanan terbaik bagi kesehatan tenaga kerja.</p>
			<h4>Misi</h4>
			<ul class="list-unstyled">
				<li><i class="fa fa-check color-green"></i> Menyelenggarakan pelayanan Medical Check Up terpadu bagi kesehatan tenaga kerja.</li>
				<li><i class="fa fa-check color-green"></i> Memiliki sumber daya manusia yang trampil, cepat, teliti , ramah dan profesional serta mampu mengikuti perkembangan ilmu dan teknologi.</li>
				<li><i class="fa fa-check color-green"></i> Menjalin dan memelihara kerjasama yang baik dengan pelanggan dan mitra kerja.</li>
				<li><i class="fa fa-check color-green"></i> Menerapkan prinsip kerja cepat dan akurat yang berfokus pada keselamatan pasien.</li>
			</ul><br />

			<h4>Motto</h4>
			<p>SEHAT AKAN MENINGKATKAN PRODUKTIFITAS</p>
		</div>

		<div class="col-md-6 text-center md-margin-bottom-40">
			<div class="responsive-video">
				<img src="{{asset('assets/img/mitralab.jpg')}}">
			</div>
		</div>
	</div><!--/row-->
</div>

<div class="container">
	<div class="headline-center margin-bottom-60">
		<h2>PELAYANAN</h2>
	</div>

	<div class="row team-v5 margin-bottom-30">
		<div class="col-sm-4 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/1.jpg')}}" alt="">
			</div>
			<span>Pendaftaran</span>
			<p>Menggunakan sistem online Clinical Information System (CIS) dan laboratory Information system (LIS)</p>
		</div>
		<div class="col-sm-4 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/2.jpg')}}" alt="">
			</div>
			<span>Sampling</span>
			<p>Memakai vacutainer, barcode, dan sekali pakai</p>
		</div>
		<div class="col-sm-4 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/3.jpg')}}" alt="">
			</div>
			<span>Konsultasi Dokter</span>
		</div>
	</div>
	<div class="headline-center margin-bottom-60">
		<h2>LABORATORIUM</h2>
		<p>SEMUA PERALATAN TERINTEGRASI DENGAN LIS</p>
	</div>
	<div class="row team-v5 margin-bottom-30">
		<div class="col-sm-4 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/4.jpg')}}" alt="">
			</div>
			<span>Pemeriksaan Kimia</span>
			<em>CHEMISTRY AUTOANALYZER</em>
		</div>
		<div class="col-sm-4 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/5.jpg')}}" alt="">
			</div>
			<span>Pemeriksaan Hematologi dan Imunologi</span>
			<em>HEMATOLOGY ANALYZER & IMMUNO ANALYZER</em>
		</div>
		<div class="col-sm-4 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/6.jpg')}}" alt="">
			</div>
			<span>Pemeriksaan Urine</span>
			<em>URINE ANALYZER</em>
		</div>
	</div>
	<div class="row team-v5 margin-bottom-30">
		<div class="col-sm-3 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/7.jpg')}}" alt="">
			</div>
			<span>Radiologi</span>
		</div>
		<div class="col-sm-3 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/8.jpg')}}" alt="">
			</div>
			<span>EKG</span>
		</div>
		<div class="col-sm-3 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/9.jpg')}}" alt="">
			</div>
			<span>Audiometri</span>
		</div>
		<div class="col-sm-3 sm-margin-bottom-50">
			<div class="team-img">
				<img style="height: 307px;" class="img-responsive img-thumbnail" src="{{asset('assets/img/pelayanan/10.jpg')}}" alt="">
			</div>
			<span>Spirometri</span>
		</div>
	</div>
</div>

<div class="text-center" style="margin-top: 20px;">
	<h2 class="title-v2 title-center">PELAYANAN KAMI</h2>
</div>
<div class="row team-v5 margin-bottom-30">
	<div class="col-sm-4 col-sm-offset-4 sm-margin-bottom-50">
			<img style="text-align: center;"  class="img-responsive profile-img margin-bottom-20" src="{{asset('assets/img/jenis_pelayanan.jpg')}}" alt="">
	</div>
</div>

<div class="text-center" style="margin-top: 20px;">
	<h2 class="title-v2 title-center">MITRA KERJA KAMI</h2>
</div>
<div class="parallax-counter-v2 parallaxBg1" style="background-position: 50% -4px;">
	<div class="container">
		<ul class="row list-row">
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. Mayora Indah</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. BOGA GROUP</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. ADESCO</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. GADING FOOD CULINARY</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. ABB SAKTI INDUSTRI</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. KDS INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. OHKUMA INDUSTRIES IND</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. TSUJIKAWA INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. SECOM INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. SECOM BHAYANGKARA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. SHIBAURA SHEARING IND.</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. FURUKAWA AUTO SYSTEMS IND.</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. NAGAI PLASTIC INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. HANWA INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. PASADENA METRIC INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. DELA CEMARA INDAH</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. MUGI LABORATORIES</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. MING HORNG INDONESIA</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. ZUIKO MACHINERY IND.</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. DAEHWA LEATHER LESTARI</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">PT. JABABEKA GROUP</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">HOTEL SAHID LIPPO CIKARANG</h4>
				</div>
			</li>
			<li class="col-md-4 col-sm-6 col-xs-12 md-margin-bottom-30">
				<div class="counters rounded">
					<h4 class="text-transform-normal">HOTEL JAVA PALACE CIKARANG</h4>
				</div>
			</li>

		</ul>
	</div>
</div>
@endsection