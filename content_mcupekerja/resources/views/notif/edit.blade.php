@extends('layout_admin')
@section('title')
Ubah Notifikasi Email - MCU Pekerja Admin Panel
@endsection

@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Ubah Notifikasi Email {{ $notif->email }}
            <small>Master data Notifikasi Email</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{route('home-admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('mst.notif.index')}}"><i class="fa fa-building active"></i> Kelola Email</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Notifikasi Email</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
                    {!! Form::open(['route' => ['mst.notif.update', $notif->id], 
                        'class' => 'form-horizontal']) !!}
                        <input type="hidden" name="_method" value="PATCH">
                      <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Alamat Email*</label>
                        <div class="col-sm-10">
                          <input value="{{old('email', $notif->email)}}" type="text" class="form-control" id="name" name="email" placeholder="Masukkan Alamat Email" required>
                          <p class="help-block" style="color:red">{{$errors->first('email')}}</p>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-info pull-right">Simpan</button>
                    {!! Form::close() !!}
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
          </div>
          <!-- Main row -->
        </section><!-- /.content -->
@endsection
@section('css')
<style>
  input[type="number"] {
    -moz-appearance: textfield;
}
input[type="number"]:hover,
input[type="number"]:focus {
    -moz-appearance: number-input;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
  margin: 0; /* Removes leftover margin */
}
</style>
@endsection
@section('js')
@endsection