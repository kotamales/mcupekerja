@extends('layout_admin')
@section('title')
Kelola Paket - MCU Pekerja Admin Panel
@endsection
@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Paket
      <small>Kelola Paket</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home-admin')}}"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Kelola Paket</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-2">
        <a href="{{route('mst.paket.create')}}" id="btnTambah" class="btn bg-navy margin">Tambah Paket</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">Daftar Paket</h3>
            </div>
          <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="tabel">
                            <thead>
                                <tr>
                                    <th>Nama Paket</th>
                                    <th>Minimal Person</th>
                                    <th>Wajib ?</th>

                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('css')
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/iCheck/all.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/admin/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/buttons.server-side.js')}}"></script>
<script src="{{asset('assets/admin/plugins/iCheck/icheck.min.js')}}"></script>
<script>
  $(document).ready(function () {
    var sukses = "{{session('tersimpan')}}";
      if(sukses == "1"){
        swal("Sukses!", "Paket berhasil tersimpan.", "success");
      }
    $('#tabel').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('src.paket') }}",
        "columns": [
            {data: 'name'},
            {data: 'minimal'},
            {data: 'wajib'},
            {data: 'action', orderable: false, searchable: false}
        ],
        "order": [[0, 'asc']]
    });
  })
  function hapus(id){
    if(confirm('Apakah Anda yakin akan hapus data Paket ini?')){
      var token = "{{csrf_token()}}";
      $.ajax({
        url: '{{ url('/mst/paket') }}' + '/' + id,
        type: "POST",
        data: {id: id, _token: token,_method:'delete'},
        success: function(result){
          swal("Sukses!", "Paket berhasil dihapus.", "success");
          var table = $('#tabel').dataTable();
          table.fnStandingRedraw();
        }
      });
    }
  }
  function ubah(id){
    window.location.href = '{{ url('/mst/paket') }}' + '/' + id + '/edit'
  }
  function detil(id){
    window.location.href = '{{ url('/tx/param') }}' + '/' + id
  }
  function setHarga(id){
    window.location.href = '{{ url('mst/paket/price/') }}' + '/' + id 
  }
</script>
@endsection