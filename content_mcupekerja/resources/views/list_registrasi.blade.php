@extends('layout_admin')
@section('title')
Laporan Registrasi - MCU Pekerja Admin Panel
@endsection
@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Registrasi
      <small>Kelola Registrasi</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home-admin')}}"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Laporan Registrasi</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">Daftar Registrasi</h3>
            </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12 text-right">
                <a id="btnExport" class="btn btn-sm btn-social btn-bitbucket" href="{{route('export-excel')}}">
                    <!-- href="{{URL::to('export-excel')}}/asd" -->
                    <i class="fa fa-download"></i> Export Excel
                  </a>
              </div>
            </div><br/>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="tabel">
                            <thead>
                                <tr>
                                    <th>Tgl</th>
                                    <th>Perusahaan</th>
                                    <th>Total Peserta</th>
                                    <th>PIC</th>
                                    <th>Telepon</th>
                                    <th>Paket</th>
                                    <th>Total Harga</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div id="modalDetil" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Data Registrasi</h4>
        </div>
        <div class="modal-body">
          <dl class="dl-horizontal">
              <dt>Nama Perusahaan</dt>
              <dd class="dd" id="d_nama_perusahaan"></dd>
              <dt>Total Peserta</dt>
              <dd class="dd" id="d_total_peserta"></dd>
              <dt>Jenis Usaha</dt>
              <dd class="dd" id="d_jenis_usaha"></dd>
              <dt>Alamat</dt>
              <dd class="dd" id="d_alamat"></dd>
              <dt>PIC</dt>
              <dd class="dd" id="d_pic"></dd>
              <dt>Telepon</dt>
              <dd class="dd" id="d_telepon"></dd>
              <dt>Email</dt>
              <dd class="dd" id="d_email"></dd>
              <dt>Paket</dt>
              <dd class="dd" id="d_paket"></dd>
              <dt>Kode Voucher</dt>
              <dd class="dd" id="d_kode_voucher"></dd>
              <dt>Cashback</dt>
              <dd class="dd" id="d_cashback"></dd>
              <dt>Tipe Cashback</dt>
              <dd class="dd" id="d_cashback_type"></dd>
              <dt>Prosentase Margin</dt>
              <dd class="dd" id="d_margin"></dd>
              <dt>Total Harga</dt>
              <dd class="dd" id="d_harga"></dd>
              <dt>Tanggal Registrasi</dt>
              <dd class="dd" id="d_created_at"></dd>
          </dl>
          <h4 class="modal-title">Lokasi</h4>
          <table class="table table-bordered">
            <tbody id="table_lokasi">
            </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection
@section('css')
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/admin/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/buttons.server-side.js')}}"></script>
<script>
  function toIndo(tgl) {
    var s = tgl.split('-');

    return s[2] + '-' + s[1] + '-'+s[0];
  }

  $(document).ready(function () {
    $('#tabel').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('data-listregistrasi') }}",
        "columns": [
            {data: 'created_at'},
            {data: 'nama_perusahaan'},
            {data: 'total_peserta'},
            {data: 'pic'},
            {data: 'telepon'},
            {data: 'paket'},
            {data: 'harga'},
            {data: 'action', orderable: false, searchable: false}
        ],
        "order": [[0, 'desc']]
    });

    $('#modalDetil').on('hidden.bs.modal', function (e) {
      $('.dd').text('');
    });

    $('div.dataTables_filter input').keyup(function(){
      var word = $(this).val();
      var link = "{{URL::to('export-excel')}}"+"/"+word;
      $('#btnExport').attr('href', link);
    });

  })

  function toDate(dateStr) {
    const [day, month, year] = dateStr.split("-");
    var _h = new Date(year, month - 1, day).getDay();
    // var _hd = _h.getday();
    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

    return hari[_h];
    
  }
  
  function detil(id){
    $.ajax({
      url: "{{URL::to('detil-registrasi')}}"+"/"+id,
      type: "GET",
      success: function(result){
        $('#d_nama_perusahaan').text(result['data']['nama_perusahaan']);
        $('#d_total_peserta').text(result['cashback']['total_peserta']);
        $('#d_jenis_usaha').text(result['data']['jenis_usaha']);
        $('#d_alamat').text(result['data']['alamat']);
        $('#d_pic').text(result['data']['pic']);
        $('#d_telepon').text(result['data']['telepon']);
        $('#d_email').text(result['data']['email']);
        $('#d_paket').html(result['data']['paket']);
        $('#d_cashback_type').text(result['data']['cashback_type']);
        $('#d_margin').text(result['data']['margin'] + ' %');
        $('#d_kode_voucher').text(result['data']['kode_voucher']);
        $('#d_created_at').text(result['data']['created_at']);
        $('#d_cashback').text('Rp '+result['cashback']['cashback']);
        $('#d_harga').text('Rp '+result['cashback']['harga']);

        // console.log(result);

        var lok = result['lok'];
        $('#table_lokasi').html('<tr><th>Kota</th><th>Alamat</th><th style="width:20%">Jumlah Peserta</th><th>PIC</th><th>Hari & Tanggal Pemeriksaan</th></tr>');
        for(var i = 0; i < lok.length; i++){
          $('#table_lokasi').append('<tr><td>'+lok[i]['name']+'</td><td>'+lok[i]['pivot']['alamat']+'</td><td>'+lok[i]['pivot']['jml']+'</td><td>'+lok[i]['pivot']['pic']+'</td><td>'+toDate(lok[i]['pivot']['tanggal'])+' '+toIndo(lok[i]['pivot']['tanggal'])+'</td></tr>');
        }

        $('#modalDetil').modal('show');
      }
    });
  }

  function hapus(id){
    if(confirm('Apakah Anda yakin akan hapus data ini?')){
      var token = "{{csrf_token()}}";
      $.ajax({
        url: "{{route('delete-registrasi')}}",
        type: "POST",
        data: {id_registrasi: id, _token: token},
        success: function(result){
          swal("Sukses!", "Data berhasil dihapus.", "success");
          var table = $('#tabel').dataTable();
          table.fnStandingRedraw();
        }
      });
    }
  }

  function pdf(id=''){
    if(id == '') {
      swal("Gagal!", "Data PDF tidak ada", "error");
      rerutn;
    }

    var win = window.open('{{ url() }}/' + id , '_blank');
    win.focus();
  }
</script>
@endsection