@extends('layout_admin')
@section('title')
Tambah Parameter - MCU Pekerja Admin Panel
@endsection

@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Parameter
            <small>Master data Parameter</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{route('home-admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('mst.parameter.index')}}"><i class="fa fa-building active"></i> Kelola Parameter</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Parameter</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
                    {!! Form::open(['route' => 'mst.parameter.store', 
                        'class' => 'form-horizontal']) !!}
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Nama Parameter*</label>
                        <div class="col-sm-10">
                          <input value="{{old('name')}}" type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama Parameter" required>
                          <p class="help-block" style="color:red">{{$errors->first('name')}}</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Minimal person</label>
                        <div class="col-sm-10">
                          <input value="{{old('minimal')}}" type="text" class="form-control" id="name" name="minimal" placeholder="Masukkan Minimal person" required>
                          <p class="help-block" style="color:red">{{$errors->first('minimal')}}</p>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-info pull-right">Tambah</button>
                    {!! Form::close() !!}
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
          </div>
          <!-- Main row -->
        </section><!-- /.content -->
@endsection
@section('css')
<style>
  input[type="number"] {
    -moz-appearance: textfield;
}
input[type="number"]:hover,
input[type="number"]:focus {
    -moz-appearance: number-input;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
  margin: 0; /* Removes leftover margin */
}
</style>
@endsection
@section('js')
@endsection