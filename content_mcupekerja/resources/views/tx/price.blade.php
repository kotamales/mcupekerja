@extends('layout_admin')
@section('title')
Harga Parameter - MCU Pekerja Admin Panel
@endsection

@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Harga Parameter {{ $parameter->name }}
            <small>seup harga</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{route('home-admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('mst.parameter.index')}}"><i class="fa fa-building active"></i> Kelola Parameter</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Setting Harga Parameter</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
                    {!! Form::open(['route' => ['mst.parameter.save']]) !!}
                      <table class="table table-bordered">
                        <thead>
                          <th>Kota</th>
                          <th>Harga < 100</th>
                          <th>Harga 100 - 300</th>
                          <th>Harga > 300</th>
                        </thead>
                        <tbody>
                        @foreach ($data as $dt)
                        <tr>
                          <input type="hidden" name="f[{{$dt->kota_id}}][kota_id]" value="{{$dt->kota_id}}">
                          <input type="hidden" name="f[{{$dt->kota_id}}][id]" value="{{$dt->id}}">
                          <input type="hidden" name="f[{{$dt->kota_id}}][parameter_id]" value="{{$dt->parameter_id}}">
                          <td>{{ $dt->name }}</td>
                          <td><input type="number" class="form-control" step="any" name="f[{{$dt->kota_id}}][harga1]" value="{{ $dt->harga1 }}"></td>
                          <td><input type="number" class="form-control" step="any" name="f[{{$dt->kota_id}}][harga2]" value="{{ $dt->harga2 }}"></td>
                          <td><input type="number" class="form-control" step="any" name="f[{{$dt->kota_id}}][harga3]" value="{{ $dt->harga3 }}"></td>
                        </tr>
                        @endforeach
                        </tbody>
                      </table>
                      <button type="submit" class="btn btn-info pull-right">Simpan</button>
                    {!! Form::close() !!}
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
          </div>
          <!-- Main row -->
        </section><!-- /.content -->
@endsection
@section('css')
<style>
  input[type="number"] {
    -moz-appearance: textfield;
}
input[type="number"]:hover,
input[type="number"]:focus {
    -moz-appearance: number-input;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
  margin: 0; /* Removes leftover margin */
}
</style>
@endsection
@section('js')
@endsection