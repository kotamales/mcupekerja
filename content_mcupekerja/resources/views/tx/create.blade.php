@extends('layout_admin')
@section('title')
Tambah Parameter - MCU Pekerja Admin Panel
@endsection

@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Parameter
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{route('home-admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('tx.param', $id)}}"><i class="fa fa-building active"></i> Kelola Parameter</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form Parameter</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
                    {!! Form::open(['route' => ['tx.save', $id], 
                        'class' => 'form-horizontal']) !!}
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Pilih  Parameter*</label>
                        <div class="col-sm-10">

                          <select id="sel" name="param" style="width:60%">
                            @foreach ($list as $l)
                              <option value="{{$l->id}}">{{$l->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-info pull-right">Tambah</button>
                    {!! Form::close() !!}
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
          </div>
          <!-- Main row -->
        </section><!-- /.content -->
@endsection
@section('css')
<style>
  input[type="number"] {
    -moz-appearance: textfield;
}
input[type="number"]:hover,
input[type="number"]:focus {
    -moz-appearance: number-input;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
  margin: 0; /* Removes leftover margin */
}
</style>
<link href="{{asset('assets/admin/plugins/select2/select2.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/iCheck/all.css')}}" rel="stylesheet">
@endsection
@section('js')
  <script src="{{asset('assets/admin/plugins/select2/select2.min.js')}}"></script>
  <script src="{{asset('assets/admin/plugins/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript">
    $("#sel").select2();
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue'
        })
</script>
@endsection