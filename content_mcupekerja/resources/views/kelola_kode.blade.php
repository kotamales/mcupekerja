@extends('layout_admin')
@section('title')
Kelola Kode Voucher - MCU Pekerja Admin Panel
@endsection
@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Kode Voucher
      <small>Kelola Kode Voucher</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home-admin')}}"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Kelola Kode Voucher</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-2">
        <a id="btnTambah" id="btnTambah" class="btn bg-navy margin">Tambah Voucher</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">Daftar Kode Voucher</h3>
            </div>
          <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="tabel">
                            <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Harga</th>
                                    <th>Valid Mulai</th>
                                    <th>Valid Sampai</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div id="modalKode" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
          <form action="{{route('save-kode')}}" method="POST">
          {!! csrf_field() !!}
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Tambah Kode Voucher</h4>
            </div>
            <div class="modal-body">
                  <label>Kode Voucher <span class="color-red">*</span></label>
                  <input id="" name="kode" type="text" class="form-control" required><br/>
                  <label>Value <span class="color-red">*</span></label>
                  <input onkeypress="return validateQty(event)" id="" name="harga" type="text" class="form-control" required><br/>
                  <label>Valid Mulai <span class="color-red">*</span></label>
                  <input id="" name="valid_mulai" type="text" class="form-control datepicker" required readonly><br/>
                  <label>Valid Sampai <span class="color-red">*</span></label>
                  <input id="" name="valid_sampai" type="text" class="form-control datepicker" required readonly><br/>
            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit">Simpan</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
          </div><!-- /.modal-content -->
          </form>
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
@endsection
@section('css')
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/admin/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/buttons.server-side.js')}}"></script>
<script src="{{asset('assets/admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script>
  $(document).ready(function () {
    var sukses = "{{session('registered')}}";
      if(sukses == "1"){
        swal("Sukses!", "Kode Voucher berhasil dibuat.", "success");
      }
      $('#btnTambah').click(function(){
        $('#modalKode').modal('show');
      });
    $('#tabel').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('data-listkode') }}",
        "columns": [
            {data: 'kode'},
            {data: 'harga'},
            {data: 'valid_mulai'},
            {data: 'valid_sampai'},
            {data: 'action', orderable: false, searchable: false}
        ],
        "order": [[0, 'asc']]
    });

    $('.datepicker').datepicker({
        todayBtn: "linked",
        format: "dd-mm-yyyy",
        todayHighlight: true,
        autoclose: true
    });
  })
  function hapus(id){
    if(confirm('Apakah Anda yakin akan hapus kode voucher ini?')){
      var token = "{{csrf_token()}}";
      $.ajax({
        url: "{{route('delete-kode')}}",
        type: "POST",
        data: {id_voucher: id, _token: token},
        success: function(result){
          swal("Sukses!", "Kode Voucher berhasil dihapus.", "success");
          var table = $('#tabel').dataTable();
          table.fnStandingRedraw();
        }
      });
    }
  }
  function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;
    if(event.which == 46)
      return true;
    else if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ( key < 48 || key > 57 ) {
        return false;
    }
    else return true;
  }
</script>
@endsection