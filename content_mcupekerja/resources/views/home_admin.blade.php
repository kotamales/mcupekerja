@extends('layout_admin')
@section('title')
MCU Pekerja - Admin
@endsection
@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            MCU Pekerja
            <small>Admin Panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard active"></i> Home</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="alert alert-success alert-dismissable">
            <h4><i class="icon fa fa-check"></i> Selamat Datang!</h4>
            Di Web Admin Panel MCUPekerja.com
          </div>

          <div class="row">
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{ $thn }}</h3>
                  <p>Registrasi Total</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('laporan-registrasi') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3>{{ $bln }}</h3>
                  <p>Registrasi Bulan Ini</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('laporan-registrasi') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{ $minggu }}</h3>
                  <p>Registrasi Mingu ini</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{ route('laporan-registrasi') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
          </div>
          
        </section><!-- /.content -->


@endsection
@section('css')
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/admin/plugins/knob/jquery.knob.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/buttons.server-side.js')}}"></script>
<script>
    $(document).ready(function () {
      // $('#tabel').DataTable({
      //       "processing": true,
      //       "serverSide": true,
      //       // "bFilter": false,
      //       "ajax": "{{-- route('jsontabledashboard') --}}",
      //       "columns": [
      //           {data: 'tgl_mulai_proyek', name: 'tgl_mulai_proyek', width: '5%'},
      //           {data: 'pekerjaan', name: 'pekerjaan', width: '15%'},
      //           {data: 'rencana_total', name: 'rencana_total', width: '10%'},
      //           {data: 'realisasi_total', name: 'realisasi_total', width: '10%'},
      //       ],
      //       "order": [[0, 'desc']]
      // });
    });
    $(function() {
        $(".dial").knob();
    });
</script>
@endsection