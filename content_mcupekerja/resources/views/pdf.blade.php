<style type="text/css">
<!--
table { vertical-align: top; }
tr    { vertical-align: top; }
td    { vertical-align: top; }
-->
</style>
<page backcolor="#FEFEFE"  backtop="10mm" backbottom="30mm"  style="font-size: 11pt;line-height: 14pt">
    <table cellspacing="0" style="width: 100%; text-align: left; font-size: 10px;line-height: 10pt">
        <tr>
            <td style="width: 35%;"></td>
            <td style="width: 35%; color: #444444;">
                <img style="width: 80%;" src="{{ asset('assets/img/logo_white.jpg') }}" alt="Logo">
            </td>
            <td style="width: 30%;"></td>
        </tr>
    </table>
    <br>
    <br>
    <table cellspacing="0" style="width: 100%; text-align: left; font-size: 11pt;">
        <tr>
            <td style="width:35%;"></td>
            <td style="width:30%;"></td>
            <td style="width:35%">Cikarang, {{ $tanggal }}</td>
        </tr>
        <tr>
            <td style="width:35%;"><br></td>
            <td style="width:30%;"></td>
            <td style="width:35%"></td>
        </tr>
        <tr>
            <td style="width:35%;">No: {{ $no or 'Nomor Register'}} </td>
            <td style="width:30%;"></td>
            <td style="width:35%"></td>
        </tr>
        {{-- <tr>
            <td style="width:35%;">Kepada Yth.</td>
            <td style="width:30%;"></td>
            <td style="width:35%"></td>
        </tr>
        <tr>
            <td style="width:35%;">{{ $reg->nama_perusahaan or ''}}</td>
            <td style="width:30%;"></td>
            <td style="width:35%"></td>
        </tr>
        <tr>
            <td style="width:35%;">{{ $reg->alamat or ''}}</td>
            <td style="width:30%;"></td>
            <td style="width:35%;"></td>
        </tr>
        <tr>
            <td style="width:35%;"><br><br></td>
            <td style="width:30%;"></td>
            <td style="width:35%"></td>
        </tr>
        <tr>
            <td style="width:35%;">UP. {{ $reg->pic or ''}}</td>
            <td style="width:30%;"></td>
            <td style="width:35%"></td>
        </tr> --}}

        <tr>
            <td colspan="3" style="width:35%;"><b>Subject : Quotation Medical Check Up</b></td>
        </tr>
    </table>
    <br>
    Yang terhormat Bapak / Ibu Pimpinan {{ $reg->nama_perusahaan or ''}}<br>
    <br>
    {{-- Dengan hormat, Terima kasih atas kesempatan yang diberikan kepada kami, menindaklanjuti pengisian formulir registrasi online dari Bapak / Ibu {{ $reg->pic or ''}},  kami dari <i>mcupekerja.com</i> mengajukan penawaran Medical Check Up (MCU) untuk Karyawan {{ $reg->nama_perusahaan or ''}}.  
    <br>
    <br> --}}
    Terima kasih atas kepercayaan anda melakukan registrasi di mcupekerja.com. Penawaran kami adalah sebagai berikut:
    <br>
    <br>
    <table cellspacing="0" cellpadding="3pt" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 9pt;">
        <tr>
            <th style="width: 20%; text-align: center">Lokasi MCU</th>
            <th style="width: 2%; text-align: center"></th>
            <th style="width: 29%; text-align: center">Paket Pilihan</th>
            <th style="width: 1%; text-align: center"></th>
            <th style="width: 13%; text-align: center;">Harga Satuan</th>
            <th style="width: 1%; text-align: center"></th>
            <th style="width: 15%; text-align: center;;">Jumlah Peserta</th>
            <th style="width: 1%; text-align: center"></th>
            <th style="width: 18%; text-align: center;;">Total Biaya</th>
        </tr>
    </table>
    @foreach ($detail as $ds=>$v)

        <table cellspacing="0" cellpadding="3pt" style="width: 100%; border: solid 1px black; background: #F7F7F7; text-align: center; font-size: 9pt;">
            @foreach ($v as $i=>$d)
            <tr>
                <td style="width: 20%; text-align: left">
                    @if ($i == 0)
                    {{ $d->alamat }}<br>{{ $d->kota }} <br>{{ ($d->tanggal2) }} {{ ($d->tanggal) }}
                    @endif
                </td>
                <td style="width: 2%; text-align: left"></td>
                <td style="width: 1%; text-align: right">-</td>
                <td style="width: 28%; text-align: left">{{$d->paket}} ( {{$d->param}} )</td>
                <td style="width: 1%; text-align: left"></td>
                <td style="width: 13%; text-align: right">Rp. 
                    @if ($reg->total_peserta <= 100 )
                        {{ number_format($d->harga1, 0, ',', '.') }}
                    @elseif ($reg->total_peserta > 100 AND $reg->total_peserta < 300)
                        {{ number_format($d->harga2, 0, ',', '.') }}
                    @else
                        {{ number_format($d->harga3, 0, ',', '.') }}
                    @endif
                </td>
                <td style="width: 1%; text-align: left"></td>
                <td style="width: 15%; text-align: right;">
                    @if ($i==0)
                        {{ number_format($d->jml, 0, ',', '.') }} Orang
                    @endif
                </td>
                <td style="width: 1%; text-align: left"></td>
                <td style="width: 18%; text-align: right;">Rp. 
                    @if ($reg->total_peserta <= 100 )
                        {{ number_format(($d->jml * $d->harga1), 0, ',', '.') }}
                    @elseif ($reg->total_peserta > 100 AND $reg->total_peserta < 300)
                        {{ number_format(($d->jml * $d->harga2), 0, ',', '.') }}
                    @else
                        {{ number_format(($d->jml * $d->harga3), 0, ',', '.') }}
                    @endif
                </td>
            </tr>
            @endforeach
        </table>
            
    @endforeach



    <table cellspacing="0" cellpadding="3pt" style="width: 100%; border: solid 1px black; background: #E7E7E7; text-align: center; font-size: 9pt;">
        <tr>
            <th style="width: 66%; text-align: right;">Total </th>
            <th style="width: 15%; text-align: right;">{{ number_format($reg->total_peserta,0, ',', '.')}} Orang</th>
            <th style="width: 1%; text-align: right;"></th>
            <th style="width: 18%; text-align: right;">Rp.
                @if ($reg->total_peserta <= 100 )
                    {{ number_format(($harga1), 0, ',', '.') }}
                @elseif ($reg->total_peserta > 100 AND $reg->total_peserta < 300)
                    {{ number_format(($harga2), 0, ',', '.') }}
                @else
                    {{ number_format(($harga3), 0, ',', '.') }}
                @endif
            </th>
        </tr>
    </table>
    <br>
    <b>Catatan: Penawaran berlaku sampai dengan {{ $tanggal2 }} (atau 1 bulan dari diterbitkan)</b>
    {{-- <br>
    Demikianlah surat penawaran ini kami buat, semoga kami menerima kabar baik dari Bapak/Ibu.
    Bila diperlukan keterangan lebih lanjut, dapat menghubungi kami :
     --}}<br>
    <br>
    Untuk menjamin kepuasan anda, kami menyediakan PIC khusus:<br> Manager Pelayanan, <b>Alfien Ubaidillah,</b> di Hp : <b>0821-1418-2782</b> dan email : <b>ubaidillahalfien@gmail.com</b>

    <br>
    <br>
    Hormat kami
    <br>
    <br>
    <br>
    <b><u>MCUPEKERJA.COM</u></b>
    {{-- <br>
    Manager Pelayanan --}}
</page>