<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
	<title>@yield('title')</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="icon" href="{{asset('assets/img/logo.jpg')}}" type="image/gif">

	<!-- Web Fonts -->
	<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

	<!-- CSS Global Compulsory -->
	<link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

	<!-- CSS Header and Footer -->
	<link rel="stylesheet" href="{{asset('assets/css/headers/header-default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/footers/footer-v1.css')}}">

	<!-- CSS Implementing Plugins -->
	<link rel="stylesheet" href="{{asset('assets/plugins/animate.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/line-icons/line-icons.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/parallax-slider/css/parallax-slider.css')}}">
	<link rel="stylesheet" href="{{asset('assets/plugins/owl-carousel/owl-carousel/owl.carousel.css')}}">

	<!-- CSS Theme -->
	<link rel="stylesheet" href="{{asset('assets/css/theme-colors/default.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/theme-skins/dark.css')}}">

	<!-- CSS Customization -->
	<link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
	<link href="{{asset('assets/admin/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet">
	<link href="{{asset('assets/admin/plugins/datepicker/datepicker3.css')}}" rel="stylesheet">
	<link href="{{asset('assets/admin/plugins/clockpicker/clockpicker.css')}}" rel="stylesheet">
	@yield('css')
</head>

<body>
	<div class="wrapper">
		<!--=== Header ===-->
		<div class="header">
			<div class="container" style="margin-bottom: 0px;">
				<!-- Logo -->
				<a class="logo" href="{{route('home')}}">
					<img style="width: 150px; margin: 5px 0 !important;" src="{{asset('assets/img/logo_white.jpg')}}" alt="Logo">
				</a>
				<!-- End Logo -->

				<!-- Topbar -->
				<div class="topbar">

					@if (Route::current()->getName() != 'register' && Route::current()->getName() != 'thanks' )
						{{-- true expr --}}
						<a href="{{route('register')}}" style="margin-top: 20px" class="pull-right btn-u btn-u-lg"><i class="fa fa-hand-o-right"></i> Daftar</a>
					@else
						
						@yield('registere')
					@endif
					{{-- <ul class="loginbar pull-right">
						<li><a href="{{route('register')}}">Register</a></li>
						<li class="topbar-devider"></li>
						<li><a href="page_login.html">Login</a></li>
					</ul> --}}
				</div>
				<!-- End Topbar -->

				<!-- Toggle get grouped for better mobile display -->
				<!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button> -->
				<!-- End Toggle -->
			</div><!--/end container-->

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
				<div class="container">
					<ul class="nav navbar-nav">
						<!-- <li class="dropdown active">
							<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
								Menu
							</a>
							<ul class="dropdown-menu">
								<li class="active">
									<a href="{{route('home')}}">Home</a>
								</li>
							</ul>
						</li> -->
					</ul>
				</div>
			</div>
		</div>
		<!--=== End Header ===-->

		@yield('content')

		<!--=== Footer Version 1 ===-->
		<div class="footer-v1">
			<div class="footer">
				<div class="container">
					<div class="row">
						<!-- About -->
						<div class="col-md-4 md-margin-bottom-20">
							<a href="{{route('home')}}"><img width="300px;" id="logo-footer" class="footer-logo" src="{{asset('assets/img/logoblack.png')}}" alt=""></a>
							{{-- <p>Selamat Datang di Platform Online Medical Check Up Pekerja Yang pertama di Indonesia!</p> --}}
						</div><!--/col-md-3-->
						<!-- End About -->

						<!-- Link List -->
						<div class="col-md-4 md-margin-bottom-20">
							<div class="headline"><h2>Useful Links</h2></div>
							<ul class="list-unstyled link-list">
								<!-- <li><a href="{{route('tentang-kami')}}">Tentang Kami</a><i class="fa fa-angle-right"></i></li> -->
								<li><a href="{{route('mitra-kami')}}">Mitra Kami</a><i class="fa fa-angle-right"></i></li>
							</ul>
						</div><!--/col-md-3-->
						<!-- End Link List -->

						<!-- Address -->
						<div class="col-md-4 md-margin-bottom-20">
							<div class="headline"><h2>Contact Us</h2></div>
							<address class="md-margin-bottom-20">
								Indonesia <br />
								Email: <a href="mailto:admin@mcupekerja.com" class="">admin@mcupekerja.com</a>
							</address>
						</div><!--/col-md-3-->
						<!-- End Address -->
					</div>
				</div>
			</div><!--/footer-->

			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p>
								2017 &copy; All Rights Reserved.
								<!-- <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a> -->
							</p>
						</div>

						<!-- Social Links -->
						{{-- <div class="col-md-6">
							<ul class="footer-socials list-inline">
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
										<i class="fa fa-facebook"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
										<i class="fa fa-google-plus"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
										<i class="fa fa-linkedin"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
										<i class="fa fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
										<i class="fa fa-instagram"></i>
									</a>
								</li>
							</ul>
						</div> --}}
						<!-- End Social Links -->
					</div>
				</div>
			</div><!--/copyright-->
		</div>
		<!--=== End Footer Version 1 ===-->
	</div><!--/wrapper-->

	

	<!-- JS Global Compulsory -->
	<script type="text/javascript" src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/jquery/jquery-migrate.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
	<!-- JS Implementing Plugins -->
	<script type="text/javascript" src="{{asset('assets/plugins/back-to-top.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/smoothScroll.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/parallax-slider/js/modernizr.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/parallax-slider/js/jquery.cslider.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/plugins/owl-carousel/owl-carousel/owl.carousel.js')}}"></script>
	<!-- JS Customization -->
	<script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>
	<!-- JS Page Level -->
	<script type="text/javascript" src="{{asset('assets/js/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/owl-carousel.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/style-switcher.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/plugins/parallax-slider.js')}}"></script>
	<script src="{{asset('assets/admin/plugins/sweetalert/sweetalert.min.js')}}"></script>

	<script src="{{asset('assets/admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('assets/admin/plugins/clockpicker/clockpicker.js')}}"></script>
	@yield('js')
	<script type="text/javascript">
		jQuery(document).ready(function() {
			App.init();
			OwlCarousel.initOwlCarousel();
			StyleSwitcher.initStyleSwitcher();
			ParallaxSlider.initParallaxSlider();
		});
		$(document).ready(function () {
    		var sukses = "{{session('registered')}}";
      		if(sukses == "1"){
      			swal("Sukses!", "Data Anda berhasil didaftarkan. Tunggu dari info kami selanjutnya!", "success");
      		}
  		});
	</script>
		<!--[if lt IE 9]>
			<script src="assets/plugins/respond.js"></script>
			<script src="assets/plugins/html5shiv.js"></script>
			<script src="assets/plugins/placeholder-IE-fixes.js"></script>
		<![endif]-->

	</body>
	</html>
