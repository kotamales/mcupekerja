@extends('layout')
@section('title')
Register
@endsection
@section('content')
<!--=== Content Part ===-->
		<div class="container content">
			<div class="row">
				<form class="reg-page" action="{{route('save-register')}}" method="POST">
					<div class="col-md-6">
							{{-- <pre>
								{{ print_r(old('kota')) }}
							</pre> --}}
							<div class="reg-header">
								<h2>Pendaftaran MCU</h2>
								<p>Lengkapi data perusahaan anda dibawah ini. </p>
							</div>
							{!! csrf_field() !!}
							<label>Nama Perusahaan (PT) <span class="color-red">*</span></label>
							<input placeholder="Masukkan Nama Perusahaan" id="nama_perusahaan" name="nama_perusahaan" type="text" class="form-control" value="{{old('nama_perusahaan')}}">
							<p class="help-block margin-bottom-20" style="color:red">{{$errors->first('nama_perusahaan')}}</p>

							<label>Jenis Usaha / Industri <span class="color-red">*</span></label>
							<input placeholder="Masukkan Jenis Usaha" id="jenis_usaha" name="jenis_usaha" type="text" class="form-control" value="{{old('jenis_usaha')}}">
							<p class="help-block margin-bottom-20" style="color:red">{{$errors->first('jenis_usaha')}}</p>


							<label>Alamat</label>
							<textarea placeholder="Masukkan Alamat Perusahaan" id="alamat_perusahaan" name="alamat" type="text" rows="3" class="form-control">{{old('alamat')}}</textarea>
							<p class="help-block margin-bottom-20" style="color:red">{{$errors->first('alamat')}}</p>

							<label>PIC Keseluruhan <span class="color-red">*</span></label>
							<input onkeypress="return alphaOnly(event)" placeholder="Masukkan PIC (Person In Charge)" id="pic" name="pic" type="text" class="form-control" value="{{old('pic')}}">
							<p class="help-block margin-bottom-20" style="color:red">{{$errors->first('pic')}}</p>

							<label>No. Telepon <span class="color-red">*</span></label>
							<input placeholder="Masukkan No. Telepon yang dapat dihubungi" onkeypress="return validateQty(event)" id="telepon" name="telepon" type="text" class="form-control" value="{{old('telepon')}}">
							<p class="help-block margin-bottom-20" style="color:red">{{$errors->first('telepon')}}</p>

							<label>Alamat Email <span class="color-red">*</span></label>
							<input placeholder="Masukkan Email" id="email" name="email" type="email" class="form-control" value="{{old('email')}}">
							<p class="help-block margin-bottom-20" style="color:red">{{$errors->first('email')}}</p>

							

							<label>Kode Voucher</label>
							<input autocomplete="off" id="kode_voucher" name="kode_voucher" type="text" class="form-control margin-bottom-20" value="{{old('kode_voucher')}}" placeholder="Masukkan Kode Voucher">
							<label>Cashback (Rp)</label>
							<div class="row">
								<div class="col-md-8"><input  autocomplete="off" id="number" name="cashback" type="text" onkeypress="return validateQty(event)" class="form-control margin-bottom-20" value="{{number_format(old('cashback',0),'0')}}" placeholder="Masukkan Cashback"></div>
								<div class="col-md-4"><select name="cashback_type" class="form-control margin-bottom-20">
                    <option value="0">Per Quotation</option>
                    <option value="1">Per Orang</option>
                  </select></div>
							</div>
							 
							

							<div id="div_kode"></div>
							<input type="hidden" name="sum_peserta" id="input_sump" value="{{ old('sum_peserta',0) }}" />
							
							<hr>
							<div class="row">
								<div class="col-lg-12 text-right">
									<button class="btn-u" type="submit">Registrasi</button>
								</div>
							</div>
					</div>
					<div class="col-md-6">

						<div class="reg-header">
							<h2>Paket</h2>
							<p>Pilih Paket. Bisa lebih dari satu.</p>
							<label>Paket yang Dipilih - Total Peserta : <span id="sum_peserta" class="color-red">{{ number_format(old('sum_peserta',0),0)}}</span></label>
							<br>
							{{-- loop paket --}}
							@foreach ($paket as $pkt)
								<div class="checkbox">
                    <input name="paketnew[]" value="{{$pkt->id}}" id="{{ str_slug($pkt->name, '_')}}"
                    	@if ($pkt->wajib == 1)
                    	 	checked="checked" disabled="disabled"
                    	@elseif(is_array(old('paketnew')) && in_array($pkt->id, old('paketnew')))
    	                    	 	checked="checked"  
                    	@else
                    	 	
                    	@endif
                    	 type="checkbox">
                    <label for="{{ str_slug($pkt->name, '_')}}">
                        {{ $pkt->name }} @if ($pkt->wajib == 1)
                    	 	(Wajib dipilih)
                    	 @endif 
                    </label>
                    @foreach ($pkt->parameter as $param)
    									<div class="checkbox">
    	                    <input name="pivot[]" value="{{$param->pivot->id}}" id="{{ str_slug($pkt->name, '_')}}-{{ str_slug($param->name)}}"
    	                    	@if ($pkt->wajib == 1)
    	                    	 	checked="checked"  
    	                    	@elseif(is_array(old('pivot')) && in_array($param->pivot->id, old('pivot')))
    	                    	 	checked="checked"  
    	                    	@else
    	                    	 	
    	                    	@endif
    	                    	disabled="disabled" type="checkbox">
    	                    <label for="{{ str_slug($pkt->name, '_')}}-{{ str_slug($param->name)}}">
    	                        {{ $param->name }}
    	                    </label>
    	                </div>
                    @endforeach
                    <br>
                </div>
							@endforeach
							<h2>Lokasi</h2>
							<p>Masukkan lokasi yang ingin diadakan. Bisa lebih dari satu.</p>
						</div>
						<button id="tambahLokasi" type="button" class="btn btn-success btn-sm">Tambah Lokasi</button>
						<div id="container-lokasi" class="form-inline">
							<?php $oldkota = old('kota'); ?>
							@if (count($oldkota) > 0)
								@foreach ($oldkota as $i => $v)
									<input class="input{{$i}} ik{{$i}}" type="hidden" value="{{$v['kota_id']}}" name="kota[{{$i}}][kota_id]">
									<input class="input{{$i}} i{{$i}}" type="hidden" value="{{$v['jml']}}" name="kota[{{$i}}][jml]">
									<input class="input{{$i}} ip{{$i}}" type="hidden" value="{{$v['pic']}}" name="kota[{{$i}}][pic]">
									<input class="input{{$i}} ikn{{$i}}" type="hidden" value="{{$v['kotane']}}" name="kota[{{$i}}][kotane]">
									<input class="input{{$i}} ia{{$i}}" type="hidden" value="{{$v['alamat']}}" name="kota[{{$i}}][alamat]">
									<input class="input{{$i}} it{{$i}}" type="hidden" value="{{$v['tanggal']}}" name="kota[{{$i}}][tanggal]">
									<input class="input{{$i}}" type="hidden" value="09:00:00" name="kota[{{$i}}][jam]">
								@endforeach
							@endif
						</div>
						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="width:25%">Lokasi</th>
									<th style="width:25%">Hari dan Tanggal</th>
									<th style="width:15%">Jumlah Peserta</th>
									<th style="width:35%">Aksi</th>
								</tr>
							</thead>
							<tbody class="c-lokasi">
								
							</tbody>
						</table>
						@if($errors->first('kota'))
						<div id="errkota" style="background: red;color: #fff;padding: 10px;">{{$errors->first('kota')}}</div>
						@endif
					</div>
				</form>
			</div>
		</div><!--/container-->
		<!--=== End Content Part ===-->
		<div id="modalFormLokasi" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
		    <div class="modal-dialog" role="document">
		      <div class="modal-content">
		        <div class="modal-header">
		         
		          <h4 class="modal-title">Tambah Lokasi</h4>
		        </div>
		        <div class="modal-body">
		          <dl class="dl-horizontal">
		          	<form id="dialogform" class="form-horizontal">
		          		<div class="form-group">
				            <label class="col-md-3 control-label" for="textinput">PIC Lokasi</label>
				            <div class="col-md-9">
				              <input name="mpic" onkeypress="return alphaOnly(event)" id="pic_lokasi" type="text" class="form-control form-lokasi" required>
				            </div>
				          </div>
		          		<div class="form-group">
				            <label class="col-md-3 control-label" for="textinput">Jumlah Peserta</label>
				            <div class="col-md-9">
				              <input name="mjml" onkeypress="return validateQty(event)" id="jml_peserta" type="text" class="form-control form-lokasi" required>
				            </div>
				          </div>
		          		<div class="form-group">
				            <label class="col-md-3 control-label" for="textinput">Alamat</label>
				            <div class="col-md-9">
				              <textarea name="malamat" id="alamat_lokasi" type="text" rows="3" class="form-control form-lokasi" required></textarea>
				            </div>
				          </div>
		          		<div class="form-group">
				            <label class="col-md-3 control-label" for="textinput">Kota</label>
				            <div class="col-md-9">
				              <select required id="mkota" name="mkotas" class="form-control form-lokasi">
  											@foreach ($kota as $k)
  												<option value="{{$k->id}}">{{$k->name}}</option>
  											@endforeach
  										</select>
				            </div>
				          </div>
		          		<div class="form-group">
				            <label class="col-md-3 control-label" for="textinput">Tanggal Pemeriksaan</label>
				            <div class="col-md-9">
				              <input name="mtgl" id="tgl_pemeriksaan" type="text" class="form-control datepicker form-lokasi" placeholder="Tanggal" required readonly>
				            </div>
				          </div>										
								</form>
		          </dl>
		        </div>
		        <div class="modal-footer">
		          <button type="button" class="btn btn-success" id="save-lokasi">Simpan</button>
		          <button id="tutupmodal" type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
		        </div>
		      </div><!-- /.modal-content -->
		    </div><!-- /.modal-dialog -->
		  </div><!-- /.modal -->
@endsection
@section('css')
	<style type="text/css">
	#container-lokasi {
		margin-bottom: 20px; 
	}
		 .checkbox {
  padding-left: 20px; }
  .checkbox label {
    display: inline-block;
    position: relative;
    padding-left: 5px; }
    .checkbox label::before {
      content: "";
      display: inline-block;
      position: absolute;
      width: 17px;
      height: 17px;
      left: 0;
      margin-left: -20px;
      border: 1px solid #cccccc;
      border-radius: 3px;
      background-color: #fff;
      -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
      -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
      transition: border 0.15s ease-in-out, color 0.15s ease-in-out; }
    .checkbox label::after {
      display: inline-block;
      position: absolute;
      width: 16px;
      height: 16px;
      left: 0;
      top: 0;
      margin-left: -20px;
      padding-left: 3px;
      padding-top: 1px;
      font-size: 11px;
      color: #555555; }
  .checkbox input[type="checkbox"] {
    opacity: 0; }
    .checkbox input[type="checkbox"]:focus + label::before {
      outline: thin dotted;
      outline: 5px auto -webkit-focus-ring-color;
      outline-offset: -2px; }
    .checkbox input[type="checkbox"]:checked + label::after {
      font-family: 'FontAwesome';
      content: "\f00c"; }
    .checkbox input[type="checkbox"]:disabled + label {
      opacity: 0.65; }
      .checkbox input[type="checkbox"]:disabled + label::before {
        background-color: #eeeeee;
        cursor: not-allowed; }
  .checkbox.checkbox-circle label::before {
    border-radius: 50%; }
  .checkbox.checkbox-inline {
    margin-top: 0; }

	.checkbox-primary input[type="checkbox"]:checked + label::before {
	  background-color: #428bca;
	  border-color: #428bca; }
	.checkbox-primary input[type="checkbox"]:checked + label::after {
	  color: #fff; }

	.checkbox-danger input[type="checkbox"]:checked + label::before {
	  background-color: #d9534f;
	  border-color: #d9534f; }
	.checkbox-danger input[type="checkbox"]:checked + label::after {
	  color: #fff; }

	.checkbox-info input[type="checkbox"]:checked + label::before {
	  background-color: #5bc0de;
	  border-color: #5bc0de; }
	.checkbox-info input[type="checkbox"]:checked + label::after {
	  color: #fff; }

	.checkbox-warning input[type="checkbox"]:checked + label::before {
	  background-color: #f0ad4e;
	  border-color: #f0ad4e; }
	.checkbox-warning input[type="checkbox"]:checked + label::after {
	  color: #fff; }

	.checkbox-success input[type="checkbox"]:checked + label::before {
	  background-color: #5cb85c;
	  border-color: #5cb85c; }
	.checkbox-success input[type="checkbox"]:checked + label::after {
	  color: #fff; }
	  label.error{color:red;}
	</style>
@endsection
@section('registere')
	<div style="margin-top: 20px;" class="pull-right text-right"><span style="font-weight: 600;font-size: 18px;color: red">Butuh bantuan?</span><br> Kami siap membantu di <span style="text-decoration: underline">No. 0821-1418-2782</span></div>

@endsection
@section('js')

<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js" type="text/javascript" />
<script type="text/javascript">

	


</script>

<script type="text/javascript">
	$(document).ready(function () {


		$('#modalFormLokasi').on('shown.bs.modal', function (e) {
		  var validator = $( "#dialogform" ).validate();
			validator.resetForm();
		})

		

		$(".reg-page").validate({
		  rules: {
		    nama_perusahaan: {
		      required: true
		    },
		    jenis_usaha: {
		      required: true
		    },
		    alamat: {
		      required: true
		    },
		    pic: {
		      required: true
		    },
		    email: {
		      required: true,
		      email: true
		    },
		    telepon: {
		      required: true
		    }
		  },
		  messages: {
		    nama_perusahaan: {
		      required: "Nama Perusahaan Wajib Di Isi"
		    },
		    jenis_usaha: {
		      required: "Jenis Usaha  Wajib Di Isi"
		    },
		    alamat: {
		      required: "Alamat Perusahaan  Wajib Di Isi"
		    },
		    pic: {
		      required: "PIC  Wajib Di Isi"
		    },
		    email: {
		      required: "Alamat Email  Wajib Di Isi",
		      email: "Alamat Email Belum Valid"
		    },
		    telepon: {
		      required: "Nomor Telepon  Wajib Di Isi"
		    }
		  }
		});

		$("#dialogform").validate({
			// onclick: false,
		  rules: {
		    mpic: {
		      required: true
		    },
		    malamat: {
		      required: true
		    },
		    mkotas: {
		      required: true
		    },
		    mtgl: {
		      required: true
		    },
		    mjml: {
		      required: true
		    }
		  },
		  messages: {
		    mpic: {
		      required: "PIC harap di isi !"
		    },
		    malamat: {
		      required: "Alamat harap di isi!"
		    },
		    mkotas: {
		      required: "Kota harap di isi!"
		    },
		    mtgl: {
		      required: "Tanggal harap di isi!"
		    },
		    mjml: {
		      required: "Jumlah Peserta harap di isi!"
		    }
		  }
		});

		

		$('input#number, input#jml_peserta').keyup(function(event) {

		  // skip for arrow keys
		  if(event.which >= 37 && event.which <= 40){
		   event.preventDefault();
		  }

		  $(this).val(function(index, value) {
		      value = value.replace(/,/g,''); // remove commas from existing input
		      return numberWithCommas(value); // add commas back in
		  });
		});


		$('#tambahLokasi').click(function(){
			$('#modalFormLokasi').modal('show');
			$('.form-lokasi').val('');
			$('#tutupmodal').show();
		});

		$('.clockpicker').clockpicker({
			autoclose :true,
			placement: 'top',
		});
		$('.datepicker').datepicker({
	          todayBtn: "linked",
	          startDate:new Date(),
	          format: "dd-mm-yyyy",
	          todayHighlight: true,
	          autoclose: true
      	}).on('hide', function(e) {
	        $('#dialogform').valid();
	    }).on('changeDate', function(e) {
	        $('#dialogform').valid();
	    });

		@foreach ($paket as $p)
	  	$("[id={{str_slug($p->name,'_')}}]").change(function() {
				if(this.checked) {
		    		$("[id^={{str_slug($p->name,'_')}}-]").prop('checked', true);
				}else{
		    		$("[id^={{str_slug($p->name,'_')}}-]").prop('checked', false);
				}
			});
		@endforeach
		
		$('#save-lokasi').click(function(){
			$('#dialogform').valid();

			var rand = Math.floor((Math.random() * 1000) + 4);
			var pic_lokasi = $('#pic_lokasi').val();
			var jml_peserta = $('#jml_peserta').val().replace(/,/g,'');
			var alamat_lokasi = $('#alamat_lokasi').val();
			var tgl_pemeriksaan = $('#tgl_pemeriksaan').val();
			var jam_pemeriksaan = $('#jam_pemeriksaan').val();
			var kota = $('#mkota').find(":selected").text();
			var idkota = $('#mkota').find(":selected").val();
			if(pic_lokasi == '') {
				// alert('PIC Lokasi Harus diisi!');

				// return;
			}
			else if(alamat_lokasi == '') {
				// alert('Alamat Harus Di Isi!');
				return;
			}
			else if(tgl_pemeriksaan == '') {
				// alert('Tanggal Pemeriksaan Harus Di Isi!');
				return;
			}
			else if(kota == '') {
				// alert('Kota belum dipilih!');
				return;
			}
			else if(parseInt(jml_peserta.replace(/,/g,'')) < 1) {
				alert('Jumlah peserta minimal 1 Orang!');
				return;
			}
			else{
				$('#errkota').remove();

				if(jml_peserta == '')
					jml_peserta = 0;
				var sum_peserta = parseInt($('#input_sump').val());
				sum_peserta = sum_peserta + parseInt(jml_peserta.replace(/,/g,''));
				$('#sum_peserta').text(numberWithCommas(sum_peserta));
				$('#input_sump').val(sum_peserta);

				lihatMinimal(sum_peserta);
			
				$('#container-lokasi').append( ''
						+ '<input class="input'+rand+' ik'+rand+'" type="hidden" value="' 
						+ idkota +'" name="kota['+rand+'][kota_id]">'
						+ '<input class="input'+rand+' i'+rand+'" type="hidden" value="' 
						+ jml_peserta +'" name="kota['+rand+'][jml]">'
						+ '<input class="input'+rand+' ikn'+rand+'" type="hidden" value="' 
						+ kota +'" name="kota['+rand+'][kotane]">'
						+ '<input class="input'+rand+' ip'+rand+'" type="hidden" value="' 
						+ pic_lokasi +'" name="kota['+rand+'][pic]">'
						+ '<input class="input'+rand+' ia'+rand+'" type="hidden" value="' 
						+ alamat_lokasi +'" name="kota['+rand+'][alamat]">'
						+ '<input class="input'+rand+' it'+rand+'" type="hidden" value="' 
						+ tgl_pemeriksaan +'" name="kota['+rand+'][tanggal]">'
						+ '<input class="input'+rand+'" type="hidden" value="09:00:00" name="kota['+rand+'][jam]">'
				);
				

				$('.c-lokasi').append(''
					+ '<tr class="isi'+rand+'">'
					+ '<td>' + kota + '<br/>' + alamat_lokasi +' </td>'
					+ '<td>' + toDate(tgl_pemeriksaan) + ', ' + tgl_pemeriksaan + ' </td>'
					+ '<td><span class="j'+rand+'">' + numberWithCommas(jml_peserta) + '</span> Orang</td>'
					+ '<td><button onClick="deleteDiv('+rand+')" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button> Hapus <button onClick="ubahDiv('+rand+')" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></button> Ubah</td>'
					+ '</tr>');
				$('#modalFormLokasi').modal('hide');
				$('.form-lokasi').val('');

			}
		});
		@if (count($oldkota) > 0)
		@foreach ($oldkota as $i => $v)
		$('.c-lokasi').append(''
			+ '<tr class="isi{{$i}}">'
			+ '<td>{{$v['kotane']}}<br/>{{$v['alamat']}}</td>'
			+ '<td>' + toDate('{{$v['tanggal']}}') + ', ' + '{{$v['tanggal']}}' + ' </td>'
			+ '<td><span class="j{{$i}}">' + numberWithCommas({{$v['jml']}})+ '</span> Orang</td>'
			+ '<td><button onClick="deleteDiv({{$i}})" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></button> Hapus <button onClick="ubahDiv({{$i}})" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></button> Ubah</td>'
			+ '</tr>'
		);
		@endforeach
		@endif
  	});

	function numberWithCommas(x) {
		    var parts = x.toString().split(".");
		    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		    return parts.join(".");
		}

  	function deleteDiv(id){
  		var	nilai_awal = parseInt($('#input_sump').val());
  		nilai_awal = nilai_awal - parseInt($('.i'+id).val().replace(/,/g,''));
		$('#sum_peserta').text(nilai_awal);
		$('#input_sump').val(nilai_awal);

		lihatMinimal(nilai_awal);

  		$('.input'+id).remove();
  		$('.isi'+id).remove();
  	}
  	function validateEmail(email) {
	    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}

  	function ubahDiv(id){
  		var	nilai_awal = parseInt($('#input_sump').val());
  		nilai_awal = nilai_awal - parseInt($('.i'+id).val().replace(/,/g,''));
		$('#sum_peserta').text(nilai_awal);
		$('#input_sump').val(nilai_awal);

		lihatMinimal(nilai_awal);

			$('#pic_lokasi').val($('.ip'+id).val());
			$('#jml_peserta').val(numberWithCommas($('.i'+id).val()));
			$('#alamat_lokasi').val($('.ia'+id).val());
			$('#tgl_pemeriksaan').val($('.it'+id).val());
			$('#jam_pemeriksaan').val();
			$('#mkota').val($('.ik'+id).val());
			

  		$('.input'+id).remove();
  		$('.isi'+id).remove();



  		$('#tutupmodal').hide();
  		$('#modalFormLokasi').modal('show');
  	}

  	function validateQty(event) {
			    var key = window.event ? event.keyCode : event.which;
			    if(event.which == 46)
			      return true;
			    else if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39) {
			        return true;
			    }
			    else if ( key < 48 || key > 57 ) {
			        return false;
			    }
			    else return true;
			  }

	var	mcu = new Object();
	@foreach ($paket as $pkt)
		mcu.{{ str_slug($pkt->name, '_')}} = new Object();;	
		mcu.{{ str_slug($pkt->name, '_')}}.minimal = {{$pkt->minimal}};	
		mcu.{{ str_slug($pkt->name, '_')}}.wajib = {{$pkt->wajib}};	
		mcu.{{ str_slug($pkt->name, '_')}}.dis = true;	
	@endforeach

	function lihatMinimal(val) {
		@foreach ($paket as $pkt)
			mcu.{{ str_slug($pkt->name, '_')}}.dis = mcu.{{ str_slug($pkt->name, '_')}}.wajib == 1 ? true : ((val <= mcu.{{ str_slug($pkt->name, '_')}}.minimal) ? true :false);
			$("[id^={{str_slug($pkt->name,'_')}}-]").attr('disabled', mcu.{{ str_slug($pkt->name, '_')}}.dis)
			
			mcu.{{ str_slug($pkt->name, '_')}}.wajib == 1 ? '' : ((val < mcu.{{ str_slug($pkt->name, '_')}}.minimal) ? $("[id={{str_slug($pkt->name,'_')}}]").trigger('change') : '');
			

		@endforeach
	}

	function toDate(dateStr) {
    const [day, month, year] = dateStr.split("-");
		var _h = new Date(year, month - 1, day).getDay();
		// var _hd = _h.getday();
    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];

    return hari[_h];
		
	}

	$('.reg-page').submit(function(event) {
		
		event.preventDefault(); //this will prevent the default submit
		if ($(".reg-page").valid()) {
			@foreach ($paket as $pkt)
			  $("[id^={{str_slug($pkt->name,'_')}}]").attr('disabled',false)
			@endforeach

			$('input#number').val($('input#number').val().replace(/,/g,'')); 


			$(this).unbind('submit').submit(); // continue the submit unbind preventDefault
		}

	})

	function alphaOnly(evt) {
	  evt = (evt) ? evt : event;
       var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
          ((evt.which) ? evt.which : 0));
       if(charCode == 32 ) {
       	return true;
       }
       
       if (charCode > 31 && (charCode < 65 || charCode > 90) &&
          (charCode < 97 || charCode > 122)) {
          return false;
       }
       return true;
	}

</script>


@endsection