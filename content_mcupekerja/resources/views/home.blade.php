@extends('layout')
@section('title')
MCU Pekerja
@endsection
@section('content')
<!--=== Slider ===-->
		<div class="slider-inner">
			<div id="da-slider" class="da-slider">
				<div class="da-slide">
					<h2><i>SELAMAT DATANG</i> <br/> <i>DI PLATFORM ONLINE MEDICAL CHECK UP PEKERJA</i> <br/> <i>YANG PERTAMA DI INDONESIA</i> </h2>
					<!-- <div class="da-img"><img class="img-responsive" src="assets/plugins/parallax-slider/img/1.png" alt=""></div> -->
				</div>
				<!-- <div class="da-slide">
					<h2><i>Visi :</i> <br /> <i>Menjadi laboratorium klinik yang terpilih karena kualitas</i> <br/> <i>pemeriksaan dan pelayanan terbaik bagi kesehatan tenaga kerja</i></h2>	
				</div>
				<div class="da-slide">
					<h2><i>USING BEST WEB</i> <br /> <i>SOLUTIONS WITH</i> <br /> <i>HTML5/CSS3</i></h2>
					<p><i>Lorem ipsum dolor amet</i> <br /> <i>tempor incididunt ut</i> <br /> <i>veniam omnis </i></p>
					<div class="da-img"><img src="assets/plugins/parallax-slider/img/html5andcss3.png" alt="image01" /></div>
				</div> -->
				<div class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</div>
			</div>
		</div><!--/slider-->
		<!--=== End Slider ===-->
<div class="purchase">
	<div class="container overflow-h">
		<div class="row">
			<div class="col-md-9 animated fadeInLeft">
				{{-- <span>Selamat Datang di Platform Online Medical Check Up Pekerja Yang Pertama di Indonesia!</span> --}}
			</div>
			<div class="col-md-3 btn-buy animated fadeInRight">
				{{-- <a href="{{route('register')}}" class="btn-u btn-u-lg"><i class="fa fa-check"></i> Daftar</a> --}}
			</div>
		</div>
	</div>
</div>
@endsection