@extends('layout_admin')
@section('title')
Tambah User - MCU Pekerja Admin Panel
@endsection

@section('content')
<!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah User
            <small>MCU Pekerja</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{route('home-admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{route('kelola-user')}}"><i class="fa fa-people active"></i> Kelola User</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form User</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-12">
                    <form class="form-horizontal" action="{{route('save-user')}}" method="post">
                      {!! csrf_field() !!}
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Nama*</label>
                        <div class="col-sm-10">
                          <input value="{{old('name')}}" type="text" class="form-control" id="name" name="name" placeholder="Masukkan Nama" required>
                          <p class="help-block" style="color:red">{{$errors->first('name')}}</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Username*</label>
                        <div class="col-sm-10">
                          <input value="{{old('username')}}" type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username" required>
                          <p class="help-block" style="color:red">{{$errors->first('username')}}</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Password*</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="password" name="password" placeholder="Masukkan Password" required>
                          <p class="help-block" style="color:red">{{$errors->first('password')}}</p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Ulangi Password*</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Ulangi Password" required>
                          <p class="help-block" style="color:red">{{$errors->first('password_confirmation')}}</p>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-info pull-right">Simpan</button>
                    </form>
                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
          </div>
          <!-- Main row -->
        </section><!-- /.content -->
@endsection
@section('css')
<style>
  input[type="number"] {
    -moz-appearance: textfield;
}
input[type="number"]:hover,
input[type="number"]:focus {
    -moz-appearance: number-input;
}
input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none;
  margin: 0; /* Removes leftover margin */
}
</style>
@endsection
@section('js')
<script>
    $(document).ready(function () {
      $(".select2").select2();
      var sukses = "{{session('registered')}}";
      if(sukses == "1"){
        swal("Sukses!", "User berhasil dibuat.", "success");
      }
    });
</script>
@endsection