@extends('layout_admin')
@section('title')
Kelola User - MCU Pekerja Admin Panel
@endsection
@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User
      <small>Kelola User</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{route('home-admin')}}"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Kelola User</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-2">
        <a href="{{route('form-user')}}" id="btnTambah" class="btn bg-navy margin">Tambah User</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-clipboard"></i>
              <h3 class="box-title">Daftar User</h3>
            </div>
          <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="tabel">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('css')
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.css')}}" rel="stylesheet">
@endsection
@section('js')
<script src="{{asset('assets/admin/plugins/dataTables/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.responsive.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/dataTables.tableTools.min.js')}}"></script>
<script src="{{asset('assets/admin/plugins/dataTables/buttons.server-side.js')}}"></script>
<script>
  $(document).ready(function () {
    var sukses = "{{session('registered')}}";
      if(sukses == "1"){
        swal("Sukses!", "User berhasil dibuat.", "success");
      }
    $('#tabel').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "{{ route('data-listuser') }}",
        "columns": [
            {data: 'name'},
            {data: 'username'},
            {data: 'action', orderable: false, searchable: false}
        ],
        "order": [[0, 'asc']]
    });
  })
  function hapus(id){
    if(confirm('Apakah Anda yakin akan hapus user ini?')){
      var token = "{{csrf_token()}}";
      $.ajax({
        url: "{{route('delete-user')}}",
        type: "POST",
        data: {id_user: id, _token: token},
        success: function(result){
          swal("Sukses!", "User berhasil dihapus.", "success");
          var table = $('#tabel').dataTable();
          table.fnStandingRedraw();
        }
      });
    }
  }
</script>
@endsection