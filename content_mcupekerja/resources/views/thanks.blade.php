@extends('layout')
@section('title')
Terima Kasih
@endsection
@section('content')

<div class="container content">
	<div class="headline"><h2>Terima Kasih Data Anda berhasil didaftarkan</h2></div>
	<div class="row">
		<div class="col-md-3 btn-buy animated fadeInleft">
			<a href="{{url($reg->file)}}" target="_blank" class="btn-u btn-u-lg"><i class="fa fa-arrow-down"></i> Download Quotation</a>
		</div>
		<div class="col-md-6 animated fadeInLeft">
		</div>
		<div class="col-md-3 btn-buy animated fadeInRight">
			<a href="{{route('home')}}" class="btn-u btn-u-lg"><i class="fa fa-arrow-left"></i> Kembali ke Beranda</a>
		</div>
	</div>
</div>
				
@endsection
@section('css')
	
@endsection
@section('js')

@endsection