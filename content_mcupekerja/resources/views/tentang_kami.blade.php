@extends('layout')
@section('title')
Tentang Kami - MCU Pekerja
@endsection
@section('content')
<div class="container content">
	<div class="headline"><h2>Tentang Kami</h2></div>
	<div class="row margin-bottom-40">
		<div class="col-md-6 md-margin-bottom-40">
			<p>MCUPekerja memiliki visi yaitu Menjadi laboratorium klinik yang terpilih karena kualitas pemeriksaan dan pelayanan terbaik bagi kesehatan tenaga kerja.</p>
			<h4>Misi</h4>
			<ul class="list-unstyled">
				<li><i class="fa fa-check color-green"></i> Menyelenggarakan pelayanan Medical Check Up terpadu bagi kesehatan tenaga kerja.</li>
				<li><i class="fa fa-check color-green"></i> Memiliki sumber daya manusia yang trampil, cepat, teliti , ramah dan profesional serta mampu mengikuti perkembangan ilmu dan teknologi.</li>
				<li><i class="fa fa-check color-green"></i> Menjalin dan memelihara kerjasama yang baik dengan pelanggan dan mitra kerja.</li>
				<li><i class="fa fa-check color-green"></i> Menerapkan prinsip kerja cepat dan akurat yang berfokus pada keselamatan pasien.</li>
			</ul><br />

			<h4>Motto</h4>
			<p>SEHAT AKAN MENINGKATKAN PRODUKTIFITAS</p>
		</div>

		<div class="col-md-6 text-center md-margin-bottom-40">
			<div class="responsive-video">
				<img src="{{asset('assets/img/logo_white.jpg')}}">
			</div>
		</div>
	</div><!--/row-->
</div>
@endsection