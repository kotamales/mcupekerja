@extends('layout')
@section('title')
Register
@endsection
@section('content')
<!--=== Content Part ===-->
<div class="container content">
	<div class="row">
    <div class="col-md-12">
  		<form id="reg-form" class="sky-form" action="{{route('save-register')}}" method="POST">
        <header>Pendaftaran MCU</header>
        <fieldset>
          <div class="row">
            <div class="col-md-6">
              <section>
                <label class="label">Nama Perusahaan (PT)</label>
                <label class="input">
                  <i class="icon-append fa fa-asterisk"></i>
                  <input type="text" placeholder="Masukkan Nama Perusahaan" id="nama_perusahaan" name="nama_perusahaan" value="{{old('nama_perusahaan')}}">
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('nama_perusahaan')}}</strong></div>
              </section>

              <section>
                <label class="label">Jenis Usaha / Industri</label>
                <label class="input">
                  <i class="icon-append fa fa-asterisk"></i>
                  <input placeholder="Masukkan Jenis Usaha" id="jenis_usaha" name="jenis_usaha" type="text" value="{{old('jenis_usaha')}}">
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('jenis_usaha')}}</strong></div>
              </section>

              <section>
                <label class="label">Alamat</label>
                <label class="textarea">
                  <textarea placeholder="Masukkan Alamat Perusahaan" id="alamat_perusahaan" name="alamat" type="text" rows="2">{{old('alamat')}}</textarea>
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('alamat')}}</strong></div>
              </section>
              <section>
                <label class="label">Alamat Email</label>
                <label class="input">
                  <i class="icon-append fa fa-asterisk"></i>
                  <input placeholder="Masukkan Email" id="email" name="email" type="email" value="{{old('email')}}">
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('email')}}</strong></div>
              </section>
            </div>
            <div class="col-md-6">
              <section>
                <label class="label">No. Telepon</label>
                <label class="input">
                  <i class="icon-append fa fa-asterisk"></i>
                  <input placeholder="Masukkan No. Telepon yang dapat dihubungi" onkeypress="return validateQty(event)" id="telepon" name="telepon" type="text" value="{{old('telepon')}}">
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('telepon')}}</strong></div>
              </section>
              <section>
                <label class="label">PIC Keseluruhan</label>
                <label class="input">
                  <i class="icon-append fa fa-asterisk"></i>
                  <input onkeypress="return alphaOnly(event)" placeholder="Masukkan PIC (Person In Charge)" id="pic" name="pic" type="text" value="{{old('pic')}}">
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('pic')}}</strong></div>
              </section>

              <section>
                <label class="label">Kode Voucher</label>
                <label class="input">
                  <input autocomplete="off" id="kode_voucher" name="kode_voucher" type="text" value="{{old('kode_voucher')}}" placeholder="Masukkan Kode Voucher">
                </label>
                <div class="note" style="color:red"><strong>{{$errors->first('kode_voucher')}}</strong></div>
              </section>

              <section>
                <label class="label">Cashback (Rp)</label>
                <div class="row">
                <label class="input col col-8">
                  <input autocomplete="off" id="number" name="cashback" type="text" onkeypress="return validateQty(event)" value="{{number_format(old('cashback',0),'0')}}" placeholder="Masukkan Cashback">
                </label>
                <label class="select col col-4">
                  <select name="cashback_type">
                    <option value="0">Per Quotation</option>
                    <option value="1">Per Orang</option>
                  </select>
                  <i></i>
                </label>
                </div>
                <div class="note" style="color:red"><strong>{{$errors->first('cashback')}}</strong></div>
              </section>
            </div>
          </div>
        </fieldset>
        <script type="text/javascript">
          var kota_i = 1;
        </script>
        <header>Lokasi MCU</header>
        <div class="lokasi kota_1">
          <fieldset>
            <div class="row">
              <div class="col-md-4">
                <section>
                  <label class="label">PIC Lokasi</label>
                  <label class="input">
                    <i class="icon-append fa fa-asterisk"></i>
                    <input onkeypress="return alphaOnly(event)" placeholder="Masukkan PIC (Person In Charge)" id="pic" name="pic" type="text" value="{{old('pic')}}">
                  </label>
                </section>
                
              </div>

              <div class="col-md-4">
                <section>
                  <label class="label">PIC Lokasi</label>
                  <label class="input">
                    <i class="icon-append fa fa-asterisk"></i>
                    <input onkeypress="return alphaOnly(event)" placeholder="Masukkan PIC (Person In Charge)" id="pic" name="pic" type="text" value="{{old('pic')}}">
                  </label>
                </section>
                <section>
                  <label class="label">Decimal number validation</label>
                  <label class="input">
                    <i class="icon-append fa fa-asterisk"></i>
                    <input type="text" name="number">
                  </label>
                </section>
                <section>
                  <label class="label">Decimal number validation</label>
                  <label class="input">
                    <i class="icon-append fa fa-asterisk"></i>
                    <input type="text" name="number">
                  </label>
                </section>
              </div>

              <div class="col-md-4">
                <section>
                  <label class="label">Pilih Parameter</label>
                  @foreach ($parameter as $p)
                    <label class="checkbox"><input type="checkbox" name="{{$p->name}}"><i></i>{{$p->name}}</label>
                  @endforeach
                </section>
              </div>

            </div>
          </fieldset>
        </div>

        <footer>
          <button type="submit" class="btn-u">Log in</button>
          <a href="#" class="btn-u btn-u-default">Register</a>
        </footer>
  		</form>
    </div>
	</div>
</div><!--/container-->
		
@endsection
@section('css')
  <link rel="stylesheet" href="{{asset('assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css')}}">
  <link rel="stylesheet" href="{{asset('assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css')}}">
@endsection
@section('registere')
	<div style="margin-top: 20px;" class="pull-right text-right"><span style="font-weight: 600;font-size: 18px;color: red">Butuh bantuan?</span><br> Kami siap membantu di <span style="text-decoration: underline">No. 0821-1418-2782</span></div>

@endsection
@section('js')

<script type="text/javascript" src="{{asset('assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/sky-forms-pro/skyforms/js/jquery.maskedinput.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/sky-forms-pro/skyforms/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js')}}"></script>

@endsection
