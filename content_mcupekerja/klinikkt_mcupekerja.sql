-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 25, 2017 at 10:11 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `klinikkt_mcupekerja`
--

-- --------------------------------------------------------

--
-- Table structure for table `kode_voucher`
--

CREATE TABLE `kode_voucher` (
  `id_voucher` int(11) NOT NULL,
  `kode` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `is_valid` int(11) NOT NULL,
  `registrasi_id` int(11) DEFAULT NULL,
  `valid_mulai` date NOT NULL,
  `valid_sampai` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kode_voucher`
--

INSERT INTO `kode_voucher` (`id_voucher`, `kode`, `harga`, `is_valid`, `registrasi_id`, `valid_mulai`, `valid_sampai`, `created_at`, `updated_at`) VALUES
(1, 'ABC123', 100000, 0, NULL, '2017-10-11', '2017-10-24', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE `lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `registrasi_id` int(11) NOT NULL,
  `pic_lokasi` varchar(100) NOT NULL,
  `jml_peserta` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `tgl_pemeriksaan` date NOT NULL,
  `jam_pemeriksaan` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`id_lokasi`, `registrasi_id`, `pic_lokasi`, `jml_peserta`, `alamat`, `tgl_pemeriksaan`, `jam_pemeriksaan`, `created_at`, `updated_at`) VALUES
(7, 17, 'Munir', 50, 'Karangpilang', '2017-10-12', '19:30:00', '2017-10-10 18:47:09', '2017-10-10 18:47:09'),
(9, 17, 'Mustofa', 50, 'ITS Sukolilo', '2017-10-31', '19:30:00', '2017-10-12 18:46:15', '2017-10-12 18:46:15'),
(10, 19, 'Didi', 30, 'Cikampek', '2017-10-04', '03:50:00', '2017-10-13 19:58:07', '2017-10-13 19:58:07');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `id_registrasi` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `total_pekerja` int(11) NOT NULL,
  `jenis_usaha` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pic` varchar(50) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `paket` text NOT NULL,
  `total_peserta` int(11) NOT NULL,
  `kode_voucher` varchar(50) NOT NULL,
  `kode_id` int(11) DEFAULT NULL,
  `harga_voucher` int(11) NOT NULL DEFAULT '0',
  `cashback` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`id_registrasi`, `nama_perusahaan`, `total_pekerja`, `jenis_usaha`, `alamat`, `pic`, `telepon`, `email`, `paket`, `total_peserta`, `kode_voucher`, `kode_id`, `harga_voucher`, `cashback`, `created_at`, `updated_at`) VALUES
(17, 'PLN Persero', 2000, 'Listrik', 'Jakarta Selatan', 'Mustofa', '081232672376', 'mustovakamal@gmail.com', 'Paket A, LDL, HDL, HBA1C, EKG', 40, 'XCC333', NULL, 0, 100000, '2017-10-13 08:37:11', '2017-10-13 00:00:00'),
(18, 'Pt testing', 200, 'Abc', 'Cikarang', 'Andy', '123', 'rubenkurniawan@gmail.com', 'Paket A', 0, 'Abc123', NULL, 0, 10000000, '2017-10-13 19:53:28', '2017-10-13 19:53:28'),
(19, 'Test 2', 75, 'Ikann', 'Karawang', 'Lulu', '098', 'rubenkurniawan@gmail.com', 'Paket A', 30, '666', NULL, 0, 1000000000, '2017-10-13 19:58:07', '2017-10-13 19:58:07'),
(20, 'PT. Sun Resourceindo Profesional', 75, 'Recruitment Services', 'PT. Sun Resourceindo Profesional 22nd Floor, Prudential Center, Kota Kasablanka, Jl. Casablanca Raya Kav 88, Jakarta, 12870- Indonesia', 'Shanti Rahardjo', '02129607439', 'adityo.pegrianto@sunrecruit.com', 'Paket A', 0, '01543000', NULL, 0, 0, '2017-10-16 12:16:56', '2017-10-16 12:16:56'),
(21, 'PT Yamazaki Indonesia', 400, 'Manufactur', 'Kawasan Greenland Batavia Blok BA no 1, Deltamas, Cikarang Pusat', 'Hariyanto', '08119411299', 'hariyanto.nugroho@yamazaki.co.id', 'Paket A', 0, '01470000', NULL, 0, 1000000, '2017-10-16 16:14:57', '2017-10-16 16:14:57'),
(22, 'PT. S-IK INDONESIA', 234, 'MANUFACTURING PLASTIC COLORING AND COMPOUNDING', 'KAWASAN INDUSTRI EJIP, JL. CITANDUY 2 , PLOT 4L, CIKARANG SELATAN, BEKASI 17530', 'IRWAN RAHARJA', '08119936860', 'irwan@s-iki.co.id', 'Paket A', 0, '44350000', NULL, 0, 1000000, '2017-10-17 07:09:03', '2017-10-17 07:09:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `remember_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `name`, `remember_token`) VALUES
(1, 'mustofa', '$2y$10$CqjIDUY/4wRgoUOehvfLKe.9ICJeOOlAVwGr6iF1inIk1oJj8Cjdy', 'Mustofa Kamal', 'dRFoDYi3iOXcqIfD8YEplE8KDYWA2LZkbs90fTDpryPYEuz1j73X0WmKg3wC'),
(2, 'admin', '$2y$10$ivdj0B0B//SxEB/y44msXuQWk6KgOTh9O3fEyQ692E4jjvbg2Eklq', 'Admin', '5A8p3xfgjqGdIPs93vkTiY0BHynt4KoF81KEAaaCAaapfhGHiZ8Lcfyk0Gqm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kode_voucher`
--
ALTER TABLE `kode_voucher`
  ADD PRIMARY KEY (`id_voucher`);

--
-- Indexes for table `lokasi`
--
ALTER TABLE `lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`id_registrasi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kode_voucher`
--
ALTER TABLE `kode_voucher`
  MODIFY `id_voucher` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `lokasi`
--
ALTER TABLE `lokasi`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `id_registrasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
